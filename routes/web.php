<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('prueba', function () {
//    return view('emails.bienvenida');
// });

Route::resource('/', 'HomeController');
// Route::resource('pastores', 'PastorController');
Route::get('mi-ofrenda', 'HomeController@ofrenda');
Route::get('mi-ofrenda/{id}', 'HomeController@show');
Route::get('gracias', 'HomeController@gracias');

Auth::routes();

Route::get('pastores-u-organizacion', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('pastores-u-organizacion', 'Auth\LoginController@login');
Route::post('logout', 'PastorController@logout')->name('logout');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');


Route::post('contacto', 'HomeController@contacto');
Route::post('export', 'HomeController@export');
Route::post('suscripcion', 'HomeController@suscripcion');
Route::post('export-suscripcion', 'HomeController@exportSuscripcion');
Route::post('export-pastores', 'HomeController@exportPastores');



//BACKEND

Route::get('pastores-u-organizacion/admin', 'PastorController@admin');
Route::get('pastores-u-organizacion/estadisticas', 'PastorController@dashboard');
Route::get('pastores-u-organizacion/mi-perfil', 'PastorController@perfil');
Route::get('pastores-u-organizacion/organizacion', 'PastorController@iglesia');
Route::post('pastores-u-organizacion/admin', 'PastorController@registro');
Route::post('pastores-u-organizacion/mi-perfil', 'PastorController@store');
Route::post('pastores-u-organizacion/organizacion', 'PastorController@storeiglesia');
Route::get('verificacion-exitosa/{id}', 'PastorController@verificacion');
Route::post('search', 'HomeController@search');
Route::get('visitas/{fecha_i}/{fecha_f}', 'HomeController@visitas');




// Route::get('/home', 'HomeController@index')->name('home');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

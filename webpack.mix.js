let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.setResourceRoot('../');

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css').version()
   .sass('resources/sass/admin.scss', 'public/css')
   .scripts([
     'resources/js/vendor/smoothscroll.js',
     'resources/js/vendor/jquery.validate.min.js',
     'resources/js/pages/contacto.js',
     'resources/js/pages/global.js',

  ],'public/js/app2.js')
  .js('resources/js/pages/admin.js', 'public/js')

 .browserSync({
   proxy: "http://localhost/giveit/public/"
 });

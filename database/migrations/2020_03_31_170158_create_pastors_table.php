<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePastorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pastors', function (Blueprint $table) {
          $table->increments('id');
          $table->string('nombre', 100);
          $table->string('codigo', 100);
          $table->text('descripcion')->nullable();
          $table->string('imagen', 255)->nullable();
          $table->integer('user_id')->unsigned();
          $table->integer('iglesia_id')->unsigned()->nullable();
          $table->timestamps();

          $table->foreign('user_id')->references('id')->on('users');
          $table->foreign('iglesia_id')->references('id')->on('iglesias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pastors');
    }
}

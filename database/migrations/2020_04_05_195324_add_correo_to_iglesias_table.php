<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCorreoToIglesiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('iglesias', function (Blueprint $table) {
          $table->string('correo', 100)->after('nombre')->nullable();
          $table->string('rut', 100)->after('correo')->nullable();
          $table->string('personalidad', 100)->after('rut')->nullable();
          $table->string('tipo', 100)->after('personalidad')->nullable();
          $table->string('cuenta', 100)->after('tipo')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('iglesias', function (Blueprint $table) {
          $table->dropColumn('correo');
          $table->dropColumn('rut');
          $table->dropColumn('personalidad');
          $table->dropColumn('tipo');
          $table->dropColumn('cuenta');
        });
    }
}

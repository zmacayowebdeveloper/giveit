// $(document).on('click','#search-button',function(){
//   var query = $('#search-live').val();
//   var route = 'search';
//
//   $.ajax({
//     url: route,
//     method: 'GET',
//     data:{query:query},
//     dataType:'json',
//     success:function(data)
//     {
//
//     }
//   })
// });

$("#formSeacrh").validate({
    errorPlacement: function(error, element) {
    error.insertBefore( element );
    },
    success: function(label, element) {
        $(element).removeClass('is-invalid');
    },
    errorPlacement: function(error, element) {
        $(element).addClass('is-invalid');
    },
    invalidHandler: function(form, validator) {
        validator.focusInvalid();
    },
    submitHandler: function (form) {
        var token = $("input[name=_token]").val();
        var formData = new FormData(form);
        var url = $(form).attr('action');
        postData(token, formData, url);
    }
});

function postData(token, formData, url){
  $.ajax({
    url: url,
    method: 'POST',
    data: formData,
    dataType: 'json',
    cache: false,
    contentType: false,
    processData: false,
    headers: {
        'X-CSRF-TOKEN': token.content
    },
    success: function (response) {
      $("#rellenandoPastores").html(response);
    }
  });
}


//Menu responsive
    $(document).ready(menu);
    function menu() {
      $('#menu-icon-shape').on('click', function() {
        $('#menu-icon-shape').toggleClass('active');
        $('#top, #middle, #bottom').toggleClass('active');
        $('#overlay-nav').toggleClass('active');
      });
    }
//menufixed
  var menu = document.getElementById('menu');
  var altura = menu.offsetTop;

  window.addEventListener('scroll',function(){
    if(window.pageYOffset > altura ){
      menu.classList.add('fixed');
    }
    else{
      menu.classList.remove('fixed');
    }
  });

  var posicionActual, posicionNueva = 0;

  $(window).scroll(function(){
    posicionNueva = $(this).scrollTop();

    if(posicionNueva>posicionActual){
    $('#barra').addClass('azulBarra');
    } else if(posicionNueva<posicionActual){
    $('#barra').removeClass('azulBarra');
    }
    posicionActual=posicionNueva;
  });

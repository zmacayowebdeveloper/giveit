$(".alert-danger").click(function(){
  $(".alert-danger").css("display", "none");

});

import swal from 'sweetalert';

$("#formCompleta").validate({
    rules: {
      name: {
        required: true
      },
      email: {
        required: true,
        email: true
      },
      description: {
        required: true
      },
      nombre: {
         required: true
      },
      correo: {
        required: true,
        email: true
      },
      direccion: {
        required: true
      },
      horario: {
       required: true
      },
      rut: {
        required: true,
        minlength: 10,
        maxlength: 10
      },
      banco: {
        required: true
      },
      tipo: {
        required: true
      },
      cuenta: {
         required: true
      },
      descripcion: {
         required: true
      },

    }
    ,errorPlacement: function(error, element) {
      error.insertBefore( element );
    },

    success: function(label, element) {
        $(element).removeClass('is-invalid');

    },
    errorPlacement: function(error, element) {
        $(element).addClass('is-invalid');
        if($(".error").hasClass("is-invalid")){
          $(".alert-danger").css("display", "block");
        }
    },
    invalidHandler: function(form, validator) {
        validator.focusInvalid();
    },
    submitHandler: function (form) {
        swal("Espere...", {
          buttons: false,
        });
        var token = $("input[name=_token]").val();
        var formData = new FormData(form);
        var url = $(form).attr('action');
        postData2(token, formData, url);
    }
});

$("#formPerfil").validate({
    rules: {
      name: {
        required: true
      },
      email: {
        required: true,
        email: true
      },
      description: {
        required: true
      },

      confirmacion: {
        equalTo: "#pass"
      }

    }
    ,errorPlacement: function(error, element) {
      error.insertBefore( element );
    },

    success: function(label, element) {
        $(element).removeClass('is-invalid');
    },
    errorPlacement: function(error, element) {
        $(element).addClass('is-invalid');
        if($(".error").hasClass("is-invalid")){
          $(".alert-danger").css("display", "block");
        }
    },
    invalidHandler: function(form, validator) {
        validator.focusInvalid();
    },
    submitHandler: function (form) {
        swal("Espere...", {
          buttons: false,
        });
        var token = $("input[name=_token]").val();
        var formData = new FormData(form);
        var url = $(form).attr('action');
        postData(token, formData, url);
    }
});

$("#formIglesia").validate({
    rules: {
      nombre: {
         required: true
      },
      correo: {
        required: true,
        email: true
      },
      direccion: {
        required: true
      },
      horario: {
       required: true
      },
      rut: {
        required: true,
        minlength: 10,
        maxlength: 10
      },
      banco: {
        required: true
      },
      tipo: {
        required: true
      },
      cuenta: {
         required: true
      },
      descripcion: {
         required: true
      },

    }
    ,errorPlacement: function(error, element) {
      error.insertBefore( element );
    },

    success: function(label, element) {
        $(element).removeClass('is-invalid');
    },
    errorPlacement: function(error, element) {
        $(element).addClass('is-invalid');
        if($(".error").hasClass("is-invalid")){
          $(".alert-danger").css("display", "block");
        }
    },
    invalidHandler: function(form, validator) {
        validator.focusInvalid();
    },
    submitHandler: function (form) {
        swal("Espere...", {
          buttons: false,
        });
        var token = $("input[name=_token]").val();
        var formData = new FormData(form);
        var url = $(form).attr('action');
        postData(token, formData, url);
    }
});

function postData(token, formData, url){
  $.ajax({
    url: url,
    method: 'POST',
    data: formData,
    dataType: 'json',
    cache: false,
    contentType: false,
    processData: false,
    headers: {
        'X-CSRF-TOKEN': token.content
    },
    success: function (response) {
      alerta();
    }
  });
}

function alerta(){
    swal(
      'Excelente!',
      'Se ha actualizado con éxito.',
      'success'
    )
}

function postData2(token, formData, url){
  $.ajax({
    url: url,
    method: 'POST',
    data: formData,
    dataType: 'json',
    cache: false,
    contentType: false,
    processData: false,
    headers: {
        'X-CSRF-TOKEN': token.content
    },
    success: function (response) {
      swal(
        'Gracias por completar tu registro.',
        'Verificaremos tu información en un lapso de 3 minutos, y pronto te notificaremos por tu correo personal.',
        'success'
      )
    }
  });
}

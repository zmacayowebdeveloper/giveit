@extends('frontend.layout2')
@section('content')
@php
    $resultado = str_replace("\\", "/", $registro->imagen);
@endphp
<section class="pastoresparallax m-top">
   <div class="pastoresparallax__content">
     <div class="single-image" style="background-image:url({{ url('storage/'.$resultado) }});">
     </div>
     <div class="pastoresparallax__content__item">
       <div class="contacto__content">
         <div class="row">
           <div class="col-md-6 col-12">
             <div class="contacto__content__contactenos">
               <h1 class="contacto__content__contactenos--primero">{{ $registro->titulo }} </h1>
               <h2 class="contacto__content__contactenos--segundo">{{ $registro->descripcion_2 }}</h2>
             </div>
           </div>
           <div class="col-md-6 col-12">
             <div class="formularioPastor__content" data-aos="fade-up-right">
               <form id="formRegister" class="row" action="{{ route('register') }}" method="post">
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">

                 <div class="formulario__group col-lg-12 col-12 ">
                   <div class="filds">
                     <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" placeholder="Nombre" required autofocus>

                     @if ($errors->has('name'))
                         <span class="invalid-feedback" role="alert">
                             <strong>{{ $errors->first('name') }}</strong>
                         </span>
                     @endif
                   </div>
                 </div>
                 <div class="formulario__group col-lg-12 col-12 ">
                   <div class="filds">
                     <input type="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" placeholder="Correo" required autofocus>
                     @if ($errors->has('email'))
                         <span class="invalid-feedback" role="alert">
                             <strong>{{ $errors->first('email') }}</strong>
                         </span>
                     @endif
                   </div>
                 </div>
                  <div class="formulario__group col-lg-12 col-12">
                    <div class="filds">
                      <input type="password" class="form-control{{ $errors->has('contraseña') ? ' is-invalid' : '' }}" name="contraseña" placeholder="Contraseña" required>

                      @if ($errors->has('contraseña'))
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('contraseña') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>
                  <div class="formulario__group col-lg-12 col-12">
                    <div class="filds">
                      <input type="password" class="form-control" name="contraseña_confirmation" placeholder="Confirmar contraseña" required>
                    </div>
                  </div>

                  <div class="formulario__group col-lg-12 col-12">
                    <button type="submit" class="btn-enviar">
                        Registrate
                    </button>

                    <a class="btn btn-link" href="{{ url('pastores-u-organizacion') }}">
                        Si ya tienes una cuenta, Inicia sección
                    </a>

                  </div>
               </form>
             </div>
           </div>
         </div>
     </div>
   </div>
</section>


@endsection

@section('scripts')
<script type="text/javascript">

</script>

@endsection

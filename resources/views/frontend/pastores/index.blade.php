@extends('frontend.layout2')
@section('content')
<section class="pastoresparallax m-top">
   <div class="pastoresparallax__content">
     <div class="single-image" style="background-image:url({{ url('storage/giving/schools-educational-1.jpg') }});">
     </div>
     <div class="pastoresparallax__content__item">
       <div class="contacto__content">
         <div class="row">
           <div class="col-md-6 col-12">
             <div class="contacto__content__contactenos">
               <h1 class="contacto__content__contactenos--primero ">Inicia Sesión </h1>
               <h2 class="contacto__content__contactenos--segundo ">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. eiusmod tempor incididunt ut labore et dolore magna aliqu</h2>
             </div>
           </div>
           <div class="col-md-6 col-12">
             <div class="formularioPastor__content">
               <form id="formLogin" class="row" action="{{ route('login') }}" method="post">
                 <input type="hidden" name="_token" id="csrf_toKen" value="{{ csrf_token() }}">

                 <div class="formulario__group col-lg-12 col-12 ">
                   <div class="filds">
                     <input type="email" id="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" placeholder="Correo" required autofocus>
                     @if ($errors->has('email'))
                         <span class="invalid-feedback" role="alert">
                             <strong>{{ $errors->first('email') }}</strong>
                         </span>
                     @endif
                   </div>
                 </div>
                  <div class="formulario__group col-lg-12 col-12">
                    <div class="filds">
                      <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Contraseña" required>

                      @if ($errors->has('password'))
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('password') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>

                  <div class="formulario__group col-lg-12 col-12">
                    <!-- <button type="submit" class="btn-enviar" >Enviar</button> -->
                    <button type="submit" class="btn-enviar">
                        Inicia sesión
                    </button>

                    <a class="btn btn-link" href="{{ route('register') }}">
                        Si eres pastor, registrate aquí
                    </a>
                    @if (Route::has('password.request'))
                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            ¿Has olvidado tu contraseña?
                        </a>
                    @endif

                  </div>
               </form>

             </div>
           </div>
         </div>
     </div>
   </div>
</section>


@endsection

@section('scripts')
<script type="text/javascript">

</script>

@endsection

@extends('frontend.layout')
@section('content')
<section class="graciasparallax m-top">
   <div class="graciasparallax__content">
     <div class="single-image" style="background-image:url({{ url('storage/giving/1280x1280.jpg') }});">
     </div>
     <div class="graciasparallax__content__item">
       <div class="item">
           <div class="graciasparallax__content__item__titulo">
             <h1 class="graciasparallax__content__item__titulo--primero split" data-aos="flip-left">GRACIAS</h1>
             <!-- <h2 class="graciasparallax__content__item__titulo--segundo">la belleza de dar a la obra de Dios</h2> -->
           </div>
       </div>
     </div>
   </div>
</section>


@endsection

@section('scripts')
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.15.0/TweenMax.min.js" type="text/javascript"></script>
<script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/16327/SplitText3.min.js" type="text/javascript"></script> -->
<script type="text/javascript">
var text = $(".split");

var split = new SplitText(text);

function random(min, max){
  return (Math.random() * (max - min)) + min;
}

$(split.chars).each(function(i){
  TweenMax.from($(this), 2.5, {
    opacity: 0,
    x: random(-500, 500),
    y: random(-500, 500),
    z: random(-500, 500),
    scale: .1,
    delay: i * .02,
    // yoyo: true,
    // repeat: -1,
    // repeatDelay: 10
  });
});
</script>

@endsection

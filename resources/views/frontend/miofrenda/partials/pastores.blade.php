@if($mensaje == null)
  <div class="ofrenda__contenido__cursos">
    <div class="row">

      @foreach($pastores as $key => $pastor)
        <?php
           if(($key+1)%2 == 0){
             $movimiento = 'fade-down-left';
           }else{
             $movimiento = 'fade-down-right';
           }
         ?>
        <div class="col-lg-4 col-xl-3 col-md-6 col-12" data-aos="{{ $movimiento }}">
          <div class="ofrenda__contenido__cursos__cards">
            <a  href="{{ url('mi-ofrenda/'.$pastor->id) }}">
              <div class="ofrenda__contenido__cursos__cards__img" >
                <img src="{{ url('storage/'.$pastor->imagen) }}" alt="" class="img-fluid">
                <div class="ofrenda__contenido__cursos__cards__img__nombre">
                  <h3>Iglesia: <span>{{ $pastor->iglesia->nombre }}</span></h3>
                </div>
              </div>
            </a>
            <div class="ofrenda__contenido__cursos__cards__contenido">
              <ul>
                <li><i class="fas fa-map-marker-alt"></i> {{ $pastor->iglesia->direccion }}</li>
              </ul>
              <ul>
                <li> <span  style="font-weight:bold">Pastor:</span> {{ $pastor->codigo }} {{ $pastor->nombre }}</li>
              </ul>
            </div>
            <div class="ofrenda__contenido__cursos__cards__botones">
              <ul>
                <li class="hovermsj"><a  href="{{ url('mi-ofrenda/'.$pastor->id) }}"> <i class="fas fa-info"></i> </a> <div class="top">Para información click aquí</div></li>
                <li class="hovermsj2"><a href="#" data-toggle="modal" data-target="#exampleModal" > <i class="fas fa-heart"></i></a><div class="top2">Para ofrendar click aquí</div></li>
              </ul>

            </div>


          </div>
        </div>
      @endforeach

    </div>
  </div>
  <div class="pastores__card">
    <div class="pastores__card--paginacion">
      <div class="row">
        <div class="col-lg-3 col-md-4 productos__card__span d-flex align-items-center">
          <span>Viendo {{ $var }} de {{ $contador }}</span>
        </div>
        <div class="col-lg-9 col-md-8">
          <div class="paginacion">
            {{ $pastores->links('pagination') }}
          </div>
        </div>
      </div>
    </div>
  </div>
@else
  <div class="pastores__card">
    <div class="pastores__card--paginacion">
      <div class="row">
        <div class="col-lg-12 col-md-12 productos__card__span d-flex align-items-center">
          <p>{{ $mensaje }}</p>

        </div>

      </div>
    </div>
  </div>
@endif

@extends('frontend.layout2')
@section('content')

<section class="ofrenda__contenido  m-top">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="ofrenda__contenido__intro">
          <div class="row">
            <div class="col-lg-6">
              <h3>Encuentra a tu iglesia o a tu pastor</h3>
              <p>En el siguiente listado podrás encontrar los pastores y/o iglesia en donde desea ofrendar.</p>
            </div>
            <div class="col-lg-6">
              <div class="ofrenda__contenido__intro__filtros">
                <h5>Filtra el listado</h5>
                  <div class="col-lg-12 p-0">
                    <div class="cursos__buscador__input">
                      <form id="formSeacrh" action="{{ url('search') }}" method="post">
                        <input type="hidden" name="_token" id="csrf_toKen" value="{{ csrf_token() }}">
                        <input type="text" id="search-live" class="form-control" name="texto" placeholder="Buscar iglesia o pastor">
                        <button type="submit" id="search-button" name="button"><i class="fas fa-search"></i></button>
                      </form>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12" id="rellenandoPastores">
        @if($mensaje == null && count($pastores) != 0)
          <div class="ofrenda__contenido__cursos">
            <div class="row">

              @foreach($pastores as $key => $pastor)
                <?php
                   if(($key+1)%2 == 0){
                     $movimiento = 'fade-down-left';
                   }else{
                     $movimiento = 'fade-down-right';
                   }
                 ?>
                <div class="col-lg-4 col-xl-3 col-md-6 col-12" data-aos="{{ $movimiento }}">
                  <div class="ofrenda__contenido__cursos__cards">
                    <a  href="{{ url('mi-ofrenda/'.$pastor->id) }}">
                      <div class="ofrenda__contenido__cursos__cards__img" >
                        <img src="{{ url('storage/'.$pastor->imagen) }}" alt="" class="img-fluid">
                        <div class="ofrenda__contenido__cursos__cards__img__nombre">
                          <h3>Iglesia: <span>{{ $pastor->iglesia->nombre }}</span></h3>
                        </div>
                      </div>
                    </a>
                    <div class="ofrenda__contenido__cursos__cards__contenido">
                      <ul>
                        <li><i class="fas fa-map-marker-alt"></i> {{ $pastor->iglesia->direccion }}</li>
                      </ul>
                      <ul>
                        <li> <span  style="font-weight:bold">Pastor:</span> {{ $pastor->codigo }} {{ $pastor->nombre }}</li>
                      </ul>
                    </div>
                    <div class="ofrenda__contenido__cursos__cards__botones">
                      <ul>
                        <li class="hovermsj"><a  href="{{ url('mi-ofrenda/'.$pastor->id) }}"> <i class="fas fa-info"></i> </a> <div class="top">Para información click aquí</div></li>
                        <li class="hovermsj2"><a href="#" data-toggle="modal" data-target="#exampleModal" > <i class="fas fa-heart"></i></a><div class="top2">Para ofrendar click aquí</div></li>
                      </ul>

                    </div>


                  </div>
                </div>
              @endforeach

            </div>
          </div>
          <div class="pastores__card">
            <div class="pastores__card--paginacion">
              <div class="row">
                <div class="col-lg-3 col-md-4 productos__card__span d-flex align-items-center">
                  <span>Viendo {{ $var }} de {{ $contador }}</span>
                </div>
                <div class="col-lg-9 col-md-8">
                  <div class="paginacion">
                    {{ $pastores->links('pagination') }}
                  </div>
                </div>
              </div>
            </div>
          </div>
        @elseif(count($pastores) == 0)
          <div class="pastores__card">
            <div class="pastores__card--paginacion">
              <div class="row">
                <div class="col-lg-12 col-md-12 productos__card__span d-flex align-items-center">
                  <p>Lo sentimos, por ahora no existe ningún registro en nuestra base de datos.</p>

                </div>

              </div>
            </div>
          </div>
          @else
            <div class="pastores__card">
              <div class="pastores__card--paginacion">
                <div class="row">
                  <div class="col-lg-12 col-md-12 productos__card__span d-flex align-items-center">
                    <p>{{ $mensaje }}</p>

                  </div>

                </div>
              </div>
            </div>
        @endif
      </div>
    </div>
  </div>
</section>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">En construcción</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ¡PRÓXIMAMENTE!
        <br>
         Para brindarte un mejor servicio.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection

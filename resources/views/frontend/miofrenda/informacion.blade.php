@extends('frontend.layout2')
@section('content')

<section class="ofrenda__contenido  m-top">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="ofrenda__contenido__intro2">
          <div class="row">
            <div class="col-lg-10 offset-lg-1">
              <div class="ofrenda__contenido__intro2__p3">
                <h3>Iglesia: <span>{{ $pastor->iglesia->nombre }}</span></h3>
                <p class="split">{{ $pastor->iglesia->descripcion }}</p>
                <ul>
                  <li  data-aos="fade-left"
                   data-aos-anchor="#example-anchor"
                   data-aos-offset="500"
                   data-aos-duration="500"><i class="fas fa-map-marker-alt"></i>  {{ $pastor->iglesia->direccion }}</li>
                   <li  data-aos="fade-right"
                    data-aos-anchor="#example-anchor"
                    data-aos-offset="500"
                    data-aos-duration="500"><i class="far fa-clock"></i>  {{ $pastor->iglesia->horario }}</li>
                  <li  data-aos="fade-left"
                   data-aos-anchor="#example-anchor"
                   data-aos-offset="500"
                   data-aos-duration="500"><i class="far fa-address-card"></i> {{ $pastor->iglesia->rut }}</li>
                  <li  data-aos="fade-right"
                   data-aos-anchor="#example-anchor"
                   data-aos-offset="500"
                   data-aos-duration="500"><i class="far fa-envelope"></i> <a href="mailto:{{ $pastor->iglesia->correo  }}" style="color:black">{{ $pastor->iglesia->correo }}</a></li>
                  <li  data-aos="fade-left"
                    data-aos-anchor="#example-anchor"
                    data-aos-offset="500"
                    data-aos-duration="500"><i class="fas fa-hand-holding-usd"></i> {{ $pastor->iglesia->banco }} Cuenta {{ $pastor->iglesia->tipo }}: {{ $pastor->iglesia->cuenta }} </li>

                </ul>
              </div>
            </div>

            <div class="col-lg-10 offset-lg-1">
              <div class="ofrenda__contenido__intro2__p3">
                <h3>Pastor: <span>{{ $pastor->codigo }} {{ $pastor->nombre }}</span></h3>
                <p class="split">{{ $pastor->descripcion }}</p>

              </div>
            </div>

          </div>
        </div>
      </div>


    </div>
  </div>
</section>

@endsection

@section('scripts')

<script type="text/javascript">

</script>

@endsection

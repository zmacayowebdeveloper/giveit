@extends('frontend.layout2')
@section('content')

<section class="ofrenda__contenido  m-top">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 offset-lg-2">
        <div class="ofrenda__contenido__intro2">
          <div class="row">
            <div class="col-lg-6 offset-lg-3">
              <div class="ofrenda__contenido__intro2__p1">
                <h3>Verificación <span>Exitosa</span></h3>
                <!-- <p class="split">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> -->

              </div>
            </div>

          </div>
        </div>
      </div>


    </div>
  </div>
</section>

@endsection

@section('scripts')

<script type="text/javascript">

</script>

@endsection

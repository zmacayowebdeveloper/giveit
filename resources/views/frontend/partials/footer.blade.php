<footer class="footer">
    <div class="footer__container justify-content-center">
      <div class="row">
        <div class="col-md-6 offset-md-3">
          <div class="suscribete__content">
            <div class="formulario__content2">
              <form id="envioCorreo" name="envioCorreo" action="{{url('suscripcion')}}" method="post">
                <h3 class="formulario__content2__titulo--mediano">Formulario de suscripción</h3>
                <div class="formulario__content2__input">
                  <input type="hidden" name="_token" id="csrf_toKen" value="{{ csrf_token() }}">
                  <div class="row">
                    <div class="filds col-md-12">
                      <input type="text" name="email" placeholder="Dirección de email" value="">
                    </div>
                    <label for="email" generated="true" class="error"></label>
                  </div>
                  <div class="row">
                    <div class=" col-md-12">
                      <button type="submit" class="btn-enviar">Enviar</button>
                    </div>
                    <div class="formulario__content__terminos">
                      <!-- <input class="check-denuncia" type="checkbox" name="checked"> -->
                       <p class="label-text parrafo">
                           Al hacer clic en "Enviar" certifico que acepto <a href="#" data-toggle="modal" data-target="#modalPolitica" id="politica">
                           la política de privacidad.</a>
                       </p>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div class="row">
              <div class="col-md-12">
                <ul class="footer__container__redes">
                  <li><a href="{{ $contacteno->facebook }}" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a href="{{ $contacteno->twitter }}" target="_blank"><i class="fab fa-twitter"></i></a></li>
                  <li><a href="{{ $contacteno->linkedin }}" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                 </ul>
                 <p class="footer__item__parrafo">2020 Ofir. Todos los derechos reservados. </p>
              </div>

            </div>
          </div>
        </div>

      </div>
    </div>
</footer>

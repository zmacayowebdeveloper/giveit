<header class="header" id="menu">
  <div class="header__top" id="barra">
    <div class="container">
      <div class="row">
        <div class="col-md-12 d-flex justify-content-center" data-aos="zoom-in-up">
            <p class="header__top__link">
              <span>
                <a href="{{ url('/') }}" target="_blank">Ofir</a>
              </span>
            </p>
        </div>
      </div>
    </div>
  </div>
  <div class="header__bottom">
    <div class="container">
      <div class="box">
        <div class="row">
          <div class="col-lg-12 col-md-12 d-flex justify-content-center">
            <nav class="navegador">
              <ul class="navegador__link">
                <li>
                  <a href="{{ url('/') }}" id="home" class="links">Inicio</a>
                </li>
                <!-- <li>
                  <a href="{{ url('/') }}/gracias" id="gracias" class="links">Gracias</a>
                </li> -->
                @guest
                  <li>
                    <a href="{{ url('/') }}/pastores-u-organizacion" id="pastores" class="links">Pastores u Organización</a>
                  </li>
                @else
                  <li>
                    <a href="{{ url('/') }}/pastores-u-organizacion/estadisticas" id="pastores" class="links">Pastores u Organización</a>
                  </li>
                @endguest
                <li>
                  <a href="{{ url('/') }}/mi-ofrenda" id="miofrenda" class="links">Mi Ofrenda</a>
                </li>
                <li>
                  <a href="{{ url('/') }}#loquehacemos" id="nosotros" class="links">Nosotros</a>
                </li>
                <li>
                  <a href="{{ url('/') }}#nuestras-causas" id="nuestrascausas" class="links">Nuestras Causas</a>
                </li>
                <!-- <li>
                  <a href="{{ url('/') }}" id="cita" class="links">Cita</a>
                </li> -->
                <li>
                  <a href="{{ url('/') }}#contactenos" id="contacto" class="links">Contacto</a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>

<!-- menu responsive-->
<div class="menu__responsive">
  <div class="logo__responsive">
    <a href="{{ url('/') }}"><img src="" class="img-fluid" alt=""></a>
  </div>
  <div id="menu-icon-shape">
    <div id="menu-icon">
      <div id="top"></div>
      <div id="middle"></div>
      <div id="bottom"></div>
    </div>
  </div>
</div>
<!-- Overlay menu -->
<div id="overlay-nav">
  <div id="nav-content">
    <ul class="menu__top">
      <li><a href="{{ url('/') }}">Inicio</a></li>
      <!-- <li><a href="{{ url('/') }}/gracias">Gracias</a></li> -->
      @guest
      <li><a href="{{ url('/') }}/pastores-u-organizacion">Pastores u Organización</a></li>
      @else
      <li><a href="{{ url('/') }}/pastores-u-organizacion/estadisticas">Pastores u Organización</a></li>
      @endguest
      <li><a href="{{ url('/') }}/mi-ofrenda">Mi ofrenda</a></li>
      <li><a href="{{ url('/') }}/#loquehacemos">Nosotros</a></li>
      <li><a href="{{ url('/') }}/#nuestras-causas">Nuestras causas</a></li>
      <!-- <li><a href="{{ url('/') }}">Cita</a></li> -->
      <li><a href="{{ url('/') }}/#contactenos">Contacto</a></li>
    </ul>
  </div>
</div>

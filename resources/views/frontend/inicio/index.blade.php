@extends('frontend.layout')
@section('content')
@php
    $resultado = str_replace("\\", "/", $banner->imagen);
    $resultado2 = str_replace("\\", "/", $nosotros->imagen);
    $resultado3 = str_replace("\\", "/", $causa_2->imagen);
@endphp
<section class="bannerparallax m-top">
    <div class="bannerparallax__content">
      <div class="single-image" style="background-image:url({{ url('storage/'.$resultado) }});">
      </div>
      <div class="bannerparallax__content__item">
        <div class="item">
            <div class="bannerparallax__content__item__titulo" data-aos="fade-right">
              <h1 class="bannerparallax__content__item__titulo--primero" >{{ $banner->titulo }}</h1>
              <h2 class="bannerparallax__content__item__titulo--segundo">{{ $banner->descripcion }}</h2>
            </div>
        </div>
      </div>
    </div>

</section>

<section  class="loquehacemos">
    <div class="loquehacemos__content" id="loquehacemos" >
      <div class="row">
        <div class="col-md-12 col-12 justify-content-center">
          <div class="loquehacemos__content__titulo" >
            <h1 class="loquehacemos__content__titulo--primero" data-aos="fade-down-right">{{ $nosotros->titulo }}</h1>
            <h2 class="loquehacemos__content__titulo--segundo" data-aos="fade-down-left">{{ $nosotros->descripcion }}</h2>
            <p data-aos="zoom-in-left">{{ $nosotros->descripcion_2 }}</p>
          </div>
        </div>
      </div>
        <div class="single-image" style="background-image:url({{ url('storage/'.$resultado2) }});" data-aos="fade-up">
        </div>
    </div>
</section>

<section class="nuestrascausas" >
    <div class="nuestrascausas__content" id="nuestras-causas">
      <div class="row">
        <div class="col-md-12 col-12 justify-content-center">
          <div class="nuestrascausas__content__titulo">
            <h1 class="nuestrascausas__content__titulo--primero">{{ $causa->titulo }}</h1>
            <h2 class="nuestrascausas__content__titulo--segundo">{{ $causa->descripcion }}</h2>
            <p></p>
          </div>
        </div>
      </div>
      <div class="row">
        <?php foreach ($causas as $key => $c): ?>
          <?php
              if(($key+1)%2 == 0){
                $movimiento = 'fade-up';
              }else{
                $movimiento = 'fade-down';
              }
              $fondo = str_replace("\\", "/", $c->imagen);
           ?>

          <div class="col-lg-6 col-xs-12 single-image" data-aos="{{ $movimiento }}" style="background-image:url({{ url('storage/'.$fondo) }});">
            <div class="nuestrascausas__content__img__titulo">
              <h1 class="nuestrascausas__content__img__titulo--primero">{{ $c->titulo }}</h1>
              <h2 class="nuestrascausas__content__img__titulo--segundo">{{ $c->descripcion }}</h2>
              <div class="col-md-5 pl-0  p-button">
                <a href="#contactenos"><div class="nuestrascausas__content__img__button" >Contáctenos</div></a>
              </div>
            </div>
          </div>
        <?php endforeach; ?>
    </div>

</section>


<section class="nuestrascausas2">
     <div class="nuestrascausas2__content">
       <div class="single-image" style="background-image:url({{ url('storage/'.$resultado3) }});" >
       </div>
       <div class="nuestrascausas2__content__item">
         <div class="item">
             <div class="nuestrascausas2__content__item__titulo">
               <h1 class="nuestrascausas2__content__item__titulo--primero">{{ $causa_2->titulo }}</h1>
               <h2 class="nuestrascausas2__content__item__titulo--segundo">{{ $causa_2->descripcion }}</h2>
             </div>
         </div>
       </div>
     </div>
</section>

<section class="contacto" >
    <div class="contacto__content" id="contactenos">
      <div class="row">
        <div class="col-md-6 col-12">
          <div class="contacto__content__contactenos">
            <h1 class="contacto__content__contactenos--primero" data-aos="flip-left">Contáctenos</h1>
            <h2 class="contacto__content__contactenos--segundo">{{ $contacteno->direccion }}</h2>
            <h2 class="contacto__content__contactenos--segundo"><a href="mailto:{{ $contacteno->correo }}">{{ $contacteno->correo }}</a></h2>
            <h2 class="contacto__content__contactenos--segundo"><a href="tel://{{ $contacteno->telefono }}">{{ $contacteno->telefono }}</a></h2>
          </div>
        </div>
        <div class="col-md-6 col-12">
          <div class="formulario__content">
            <form id="formContact" class="row" action="{{ url('contacto') }}" method="post">
              <input type="hidden" name="_token" id="csrf_toKen" value="{{ csrf_token() }}">

              <div class="formulario__group col-lg-6 col-12">
                <div class="filds">
                  <input type="text" id="nombre" name="nombre" placeholder="Nombre">
                </div>
              </div>

              <div class="formulario__group col-lg-6 col-12 pl-0">
                <div class="filds">
                  <input type="email" id="email" name="email" placeholder="Email">
                </div>
              </div>
               <div class="formulario__group col-lg-6 col-12">
                 <div class="filds">
                   <input type="text" id="telefono" name="telefono" placeholder="Teléfono">
                 </div>
               </div>
               <div class="formulario__group col-lg-6 col-12 pl-0">
                 <div class="filds">
                   <input type="text" id="direccion" name="direccion" placeholder="Dirección">
                 </div>
               </div>
               <div class="formulario__group col-lg-12 col-12">
                 <div class="filds">
                   <input type="text" id="asunto" name="asunto" placeholder="Asunto">
                 </div>
               </div>
               <div class="formulario__group col-lg-12 col-12">
                 <div class="filds">
                   <textarea type="text" class="mensaje" id="mensaje" name="mensaje" placeholder="Escribe tu mensaje..."></textarea>
                 </div>
               </div>
               <div class="formulario__group col-lg-12 col-12">
                 <label class="labelerror">Campo(s) requerido(s)</label>
               </div>
               <div class="formulario__group col-lg-12 col-12" data-aos="flip-left">
                 <button type="submit" class="btn-enviar" >Enviar</button>
                 <div class="formulario__content__terminos">
                   <!-- <input class="check-denuncia" type="checkbox" name="checked"> -->
                    <p class="label-text parrafo">
                        Al hacer clic en "Enviar" certifico que acepto <a href="#" data-toggle="modal" data-target="#modalPolitica" id="politica">
                        la política de privacidad del sitio.</a>
                    </p>
                 </div>

               </div>
           </form>
          </div>
        </div>
      </div>
      
    </div>
</section>

@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function(){
    // console.log($(".error").hasClass("is-invalid"))
    $(".labelerror").css("display", "none");

  });
</script>

@endsection

<!doctype html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>Ofir</title>
<meta name="description" content="Ofir solidario">
<link rel="icon" href="{{ asset('images/favicon.ico') }}"  type="image/x-icon"/>
<link rel="stylesheet" href="{{ url('css/app.css?v=1.4') }}">
<link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous"
@yield('stylesheet')
</head>
<body>

@include('frontend.partials.header')

@yield('content')

@include('frontend.partials.politica')
@include('frontend.partials.footer')
<script src="{{ url('js/app.js?v=1.4') }}"></script>
<script type="text/javascript">
  const site_url = '{{ url('/') }}/';
</script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script src="{{ url('js/app2.js?v=1.2') }}"></script>

<script>
$(document).ready(function() {
  $('a[href^="#"]').click(function() {
    var destino = $(this.hash);

    if (destino.length == 1) {
      destino = $('#' + this.hash.substr(1));

      // console.log(destino, this.hash.substr(1))

    }
    // if (destino.length == 0) {
    //   destino = $('html');
    // }

    $('html, body').animate({ scrollTop: destino.offset().top }, 800);
    return false;
  });
});
</script>
@yield('scripts')
</body>
</html>

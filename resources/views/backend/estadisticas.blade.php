@extends('backend.layout.layout')
@section('contenido')
<div class="right_col margin-content" role="main" >
  <!-- top tiles -->
  <div class="row" style="display: inline-block; width: 25%;" >
  <div class="tile_count">

    <div class="col-md-6 col-sm-6  tile_stats_count">
      <span class="count_top"><i class="fas fa-user"></i> Total</span>
      <div class="count">{{ $total }}</div>
      <span class="count_bottom"><i class="green"></i><i class="fas fa-sort-asc"></i> Visitas</span>
    </div>
    <div class="col-md-6 col-sm-6  tile_stats_count">
      <span class="count_top"><i class="fas fa-user"></i> Hoy </span>
      <div class="count green">{{ $contador }}</div>
      <span class="count_bottom"><i class="green"><i class="fas fa-sort-asc"></i>Visitas</i> </span>
    </div>

  </div>
</div>
  <!-- /top tiles -->

  <div class="row">
    <div class="col-md-12 col-sm-12 ">
      <div class="dashboard_graph">

        <div class="row x_title">
          <div class="col-md-6">
            <h3>Visitantes  <small id="diasvisitas"></small></h3>
          </div>
          <div class="col-md-6">
            <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
              <i class="fas fa-calendar"></i>
              <span></span> <b class="caret"></b>
            </div>
          </div>
        </div>

        <div class="col-md-12 col-sm-12 ">
          <!-- <div id="chart_plot_01" class="demo-placeholder"></div> -->
                <canvas id="myChart" class="mt-4"></canvas>
        </div>
        <!-- <div class="col-md-3 col-sm-3  bg-white">
          <div class="x_title">
            <h2>Rendimiento</h2>
            <div class="clearfix"></div>
          </div>

          <div class="col-md-12 col-sm-12 ">
            <div>
              <p>Durante el presente año</p>
              <div class="">
                <div class="progress progress_sm" style="width: 76%;">
                  <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="40"></div>
                </div>
              </div>
            </div>
            <div>
              <p>Hace un mes </p>
              <div class="">
                <div class="progress progress_sm" style="width: 76%;">
                  <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="50"></div>
                </div>
              </div>
            </div>

          </div>
          <div class="col-md-12 col-sm-12 ">
            <div>
              <p>Mes Actual</p>
              <div class="">
                <div class="progress progress_sm" style="width: 76%;">
                  <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="60"></div>
                </div>
              </div>
            </div>
            <div>
              <p>Hoy</p>
              <div class="">
                <div class="progress progress_sm" style="width: 76%;">
                  <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="50"></div>
                </div>
              </div>
            </div>
          </div>

        </div> -->

        <div class="clearfix"></div>
      </div>
    </div>

  </div>
  <br />

</div>
@endsection

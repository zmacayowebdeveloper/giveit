@extends('backend.layout.layout')
@section('contenido')
<div class="right_col margin-content2" role="main">
  <div class="col-md-12">
      <div class="card">
          <div class="card-header">Organización</div>

          <div class="card-body">
              <div class="formulario__content" data-aos="fade-up-right">

                <form id="formIglesia" class="row" action="{{ url('pastores-u-organizacion/organizacion') }}" method="post">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="col-lg-12 formulario__content__top">
                  <div class="col-lg-6 col-12">
                    <div class="filds">
                      <label for="">Nombre</label>
                      <input type="text" class="form-control" name="nombre"  value="{{ $iglesia->nombre }}">
                    </div>
                  </div>
                  <div class="col-lg-6 col-12 ">
                    <div class="filds">
                      <label for="">Correo</label>
                      <input type="email" name="correo" class="form-control"  value="{{ $iglesia->correo }}">
                    </div>
                  </div>
                  <div class="col-lg-6 col-12">
                    <div class="filds">
                      <label for="">Dirección</label>
                      <input type="text" class="form-control" name="direccion"  value="{{ $iglesia->direccion }}">
                    </div>
                  </div>
                  <div class="col-lg-6 col-12 ">
                    <div class="filds">
                      <label for="">Horario</label>
                      <input type="text" name="horario" class="form-control"  value="{{ $iglesia->horario }}">
                    </div>
                  </div>
                   <div class="col-lg-6 col-12 ">
                     <div class="filds">
                       <label for="">RUT</label>
                       <input type="text" name="rut" id="rut" oninput="checkRut(this)" class="form-control" value="{{ $iglesia->rut }}">
                     </div>
                   </div>
                   <div class="col-lg-6 col-12 ">
                     <div class="filds">
                       <label for="">Personalidad Jurídica (opcional)</label>
                       <input type="text" name="personalidad" class="form-control" value="{{ $iglesia->personalidad }}">
                     </div>
                   </div>
                   <div class="col-lg-4 col-12 ">
                     <div class="filds">
                       <label for="">Banco</label>
                       <select class="form-control" name="banco">
                         <option value="ABN AMRO" @if($iglesia->banco == 'ABN AMRO') selected @endif>ABN AMRO</option>
                         <option value="Atlas - Citibank" @if($iglesia->banco == 'Atlas - Citibank') selected @endif>Atlas - Citibank</option>
                         <option value="BancaFacil - Sitio de educación bancaria" @if($iglesia->banco == 'BancaFacil - Sitio de educación bancaria') selected @endif>BancaFacil - Sitio de educación bancaria</option>
                         <option value="Banco Bice" @if($iglesia->banco == 'Banco Bice') selected @endif>Banco Bice</option>
                         <option value="Banco Central de Chile" @if($iglesia->banco == 'Banco Central de Chile') selected @endif>Banco Central de Chile</option>
                         <option value="Banco de Chile" @if($iglesia->banco == 'Banco de Chile') selected @endif>Banco de Chile</option>
                         <option value="Banco de Crédito e Inversiones" @if($iglesia->banco == 'Banco de Crédito e Inversiones') selected @endif>Banco de Crédito e Inversiones</option>
                         <option value="Banco del Desarrollo" @if($iglesia->banco == 'Banco del Desarrollo') selected @endif>Banco del Desarrollo</option>
                         <option value="Banco del Desarrollo - Asesoría Financiera" @if($iglesia->banco == 'Banco del Desarrollo - Asesoría Financiera') selected @endif>Banco del Desarrollo - Asesoría Financiera</option>
                         <option value="Banco Edwards" @if($iglesia->banco == 'Banco Edwards') selected @endif>Banco Edwards</option>
                         <option value="Banco Falabella" @if($iglesia->banco == 'Banco Falabella') selected @endif>Banco Falabella</option>
                         <option value="Banco Internacional" @if($iglesia->banco == 'Banco Internacional') selected @endif>Banco Internacional</option>
                         <option value="Banco Nova" @if($iglesia->banco == 'Banco Nova') selected @endif>Banco Nova</option>
                         <option value="Banco Penta" @if($iglesia->banco == 'Banco Penta') selected @endif>Banco Penta</option>
                         <option value="Banco Santander Santiago" @if($iglesia->banco == 'Banco Santander Santiago') selected @endif>Banco Santander Santiago</option>
                         <option value="Banco Security" @if($iglesia->banco == 'Banco Security') selected @endif>Banco Security</option>
                         <option value="BancoEstado" @if($iglesia->banco == 'BancoEstado') selected @endif>BancoEstado</option>
                         <option value="Citibank N.A. Chile" @if($iglesia->banco == 'Citibank N.A. Chile') selected @endif>Citibank N.A. Chile</option>
                         <option value="BBVA" @if($iglesia->banco == 'BBVA') selected @endif>BBVA</option>
                         <option value="Corpbanca" @if($iglesia->banco == 'Corpbanca') selected @endif>Corpbanca</option>
                         <option value="Credichile" @if($iglesia->banco == 'Credichile') selected @endif>Credichile</option>
                         <option value="Credit Suisse Consultas y Asesorias Limitada" @if($iglesia->banco == 'Credit Suisse Consultas y Asesorias Limitada') selected @endif>Credit Suisse Consultas y Asesorias Limitada</option>
                         <option value="Deutsche Bank" @if($iglesia->banco == 'Deutsche Bank') selected @endif>Deutsche Bank</option>
                         <option value="ING Bank N.V." @if($iglesia->banco == 'ING Bank N.V.') selected @endif>ING Bank N.V.</option>
                         <option value="Redbanc" @if($iglesia->banco == 'Redbanc') selected @endif>Redbanc</option>
                         <option value="Santander Banefe" @if($iglesia->banco == 'Santander Banefe') selected @endif>Santander Banefe</option>
                         <option value="Scotiabank Sud Americano" @if($iglesia->banco == 'Scotiabank Sud Americano') selected @endif>Scotiabank Sud Americano</option>
                         <option value="UBS AG in Santiago de Chile" @if($iglesia->banco == 'UBS AG in Santiago de Chile') selected @endif>UBS AG in Santiago de Chile</option>
                       </select>

                     </div>
                   </div>
                   <div class="col-lg-4 col-12 ">
                     <div class="filds">
                       <label for="">Tipo de cuenta</label>
                       <select class="form-control" name="tipo">
                         <option value="Vista" @if($iglesia->tipo == 'Vista') selected @endif>Vista</option>
                         <option value="Rut" @if($iglesia->tipo == 'Rut') selected @endif>RUT</option>
                         <option value="Corriente" @if($iglesia->tipo == 'Corriente') selected @endif>Corriente</option>
                       </select>
                     </div>
                   </div>
                   <div class="col-lg-4 col-12 ">
                     <div class="filds">
                       <label for="">N° de cuenta</label>
                       <input type="text" name="cuenta" class="form-control" value="{{ $iglesia->cuenta }}">
                     </div>
                   </div>
                   <div class="col-lg-12 col-12 ">
                     <div class="filds">
                       <label for="">Descripción</label>
                       <textarea class="form-control" name="descripcion" value="{{ $iglesia->descripcion }}">{{ $iglesia->descripcion }}</textarea>
                     </div>
                   </div>
                 </div>
                 <div class="col-lg-12 formulario__content__top">
                  <div class="col-lg-4 col-12">
                    <button type="submit" class="btn btn-primary btn-enviar">
                        Enviar
                    </button>
                  </div>
                </div>
                </form>
              </div>
              <div class="alert alert-danger" role="alert">
                  Campo(s) requerido(s)
              </div>
          </div>
      </div>
  </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
function checkRut(rut) {
    // Despejar Puntos
    var valor = rut.value.replace('.','');
    // Despejar Guión
    valor = valor.replace('-','');

    // Aislar Cuerpo y Dígito Verificador
    cuerpo = valor.slice(0,-1);
    dv = valor.slice(-1).toUpperCase();

    // Formatear RUN
    rut.value = cuerpo + '-'+ dv

    // Si no cumple con el mínimo ej. (n.nnn.nnn)
    if(cuerpo.length < 7) { rut.setCustomValidity("RUT Incompleto"); return false;}

    // Calcular Dígito Verificador
    suma = 0;
    multiplo = 2;

    // Para cada dígito del Cuerpo
    for(i=1;i<=cuerpo.length;i++) {

        // Obtener su Producto con el Múltiplo Correspondiente
        index = multiplo * valor.charAt(cuerpo.length - i);

        // Sumar al Contador General
        suma = suma + index;

        // Consolidar Múltiplo dentro del rango [2,7]
        if(multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }

    }

    // Calcular Dígito Verificador en base al Módulo 11
    dvEsperado = 11 - (suma % 11);

    // Casos Especiales (0 y K)
    dv = (dv == 'K')?10:dv;
    dv = (dv == 0)?11:dv;

    // Validar que el Cuerpo coincide con su Dígito Verificador
    if(dvEsperado != dv) { rut.setCustomValidity("RUT Inválido"); return false; }

    // Si todo sale bien, eliminar errores (decretar que es válido)
    rut.setCustomValidity('');
}
</script>
@endsection

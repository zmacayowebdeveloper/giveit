<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="icon" href="images/favicon.ico" type="image/ico" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <title>Ofir</title>

    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">

    <!-- Bootstrap -->
    <link href="{{ asset('css/vendor/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('css/vendor/font-awesome.min.css') }}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset('css/vendor/nprogress.css') }}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{ asset('css/vendor/green.css') }}" rel="stylesheet">

    <!-- bootstrap-progressbar -->
    <link href="{{ asset('css/vendor/bootstrap-progressbar-3.3.4.min.css') }}" rel="stylesheet">
    <!-- JQVMap -->
    <link href="{{ asset('css/vendor/jqvmap.min.css') }}" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="{{ asset('css/vendor/daterangepicker.css') }}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{ asset('css/vendor/custom.min.css') }}" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        @include('backend.partials.sidebar')

        <!-- top navigation -->
        @include('backend.partials.header')
        <!-- /top navigation -->


        <!-- page content -->
        @yield('contenido')
        <!-- /page content -->

        <!-- footer content -->
        @include('backend.partials.footer')
        <!-- /footer content -->
      </div>
    </div>
     <script type="text/javascript">
     const site_url = '{{ url('/') }}/';
       
     </script>
    <!-- jQuery -->
    <script src="{{ asset('js/vendor/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('js/vendor/bootstrap.bundle.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('js/vendor/fastclick.js') }}"></script>
    <!-- NProgress -->
    <script src="{{ asset('js/vendor/nprogress.js') }}"></script>
    <!-- Chart.js -->
    <script src="{{ asset('js/vendor/Chart.min.js') }}"></script>

    <script src="{{ asset('js/vendor/jquery.sparkline.min.js') }}"></script>
    <!-- gauge.js -->
    <script src="{{ asset('js/vendor/gauge.min.js') }}"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{ asset('js/vendor/bootstrap-progressbar.min.js') }}"></script>
    <!-- iCheck -->
    <script src="{{ asset('js/vendor/icheck.min.js') }}"></script>
    <!-- Skycons -->
    <script src="{{ asset('js/vendor/skycons.js') }}"></script>
    <!-- Flot -->
    <script src="{{ asset('js/vendor/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('js/vendor/flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('js/vendor/flot/jquery.flot.time.js') }}"></script>
    <script src="{{ asset('js/vendor/flot/jquery.flot.stack.js') }}"></script>
    <script src="{{ asset('js/vendor/flot/jquery.flot.resize.js') }}"></script>
    <!-- Flot plugins -->
    <script src="{{ asset('js/vendor/flot/jquery.flot.orderBars.js') }}"></script>
    <script src="{{ asset('js/vendor/flot/jquery.flot.spline.min.js') }}"></script>
    <script src="{{ asset('js/vendor/flot/curvedLines.js') }}"></script>
    <!-- DateJS -->
    <script src="{{ asset('js/vendor/date.js') }}"></script>
    <!-- JQVMap -->
    <script src="{{ asset('js/vendor/jqvmap/jquery.vmap.js') }}"></script>
    <script src="{{ asset('js/vendor/jqvmap/jquery.vmap.world.js') }}"></script>
    <script src="{{ asset('js/vendor/jqvmap/jquery.vmap.sampledata.js') }}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{ asset('js/vendor/moment.min.js') }}"></script>
    <script src="{{ asset('js/vendor/daterangepicker.js') }}"></script>

    <!-- Custom Theme Scripts -->
    <script src="{{ asset('js/vendor/custom.min.js') }}"></script>
    <script src="{{ url('js/app2.js') }}"></script>
    <script src="{{ url('js/admin.js') }}"></script>
    @yield('scripts')

  </body>
</html>

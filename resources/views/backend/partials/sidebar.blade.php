<div class="col-md-3 left_col">
  <div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
      <a href="{{ url('pastores-u-organizacion/admin') }}" class="site_title"><i class="fab fa-opera"></i> <span>Ofir</span></a>
    </div>

    <div class="clearfix"></div>

    <!-- menu profile quick info -->
    <div class="profile clearfix">
      <div class="profile_pic">
        <!-- <img src="{{ url('storage/'.Auth::user()->avatar) }}" alt="..." class="profile_img2"> -->
      </div>
      <div class="profile_info">
        <span>Bienvenido,</span>
        <h2>{{ Auth::user()->name }}</h2>
      </div>
    </div>
    <!-- /menu profile quick info -->

    <br />

    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
      <div class="menu_section">
        <h3>Admin Pastor</h3>
        <ul class="nav side-menu">
          <li><a><i class="fas fa-home"></i> Administra<span class="fas fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="{{ url('pastores-u-organizacion/estadisticas') }}">Estadísticas</a></li>
              <li><a href="{{ url('pastores-u-organizacion/mi-perfil') }}">Sobre mí</a></li>
              <li><a href="{{ url('pastores-u-organizacion/organizacion') }}">Organización</a></li>
            </ul>
          </li>

        </ul>
      </div>

    </div>
    <!-- /sidebar menu -->

    <!-- /menu footer buttons -->
    <div class="sidebar-footer hidden-small pull-right">

      <a data-toggle="tooltip" data-placement="top" title="Cerrar sesión" href="{{ route('logout') }}"
         onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">
        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
      </a>
      <form action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
      </form>
    </div>
    <!-- /menu footer buttons -->
  </div>
</div>

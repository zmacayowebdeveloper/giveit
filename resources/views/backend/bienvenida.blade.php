@extends('backend.layout.layout')
@section('contenido')
<div class="right_col" role="main">
  <div class="col-md-12">
      <div class="card">
          <div class="card-header">Bienvenido {{ Auth::user()->name }}!</div>

          <div class="card-body">

              <div class="formulario__content" data-aos="fade-up-right">
                <h3>Completa tu registro</h3>
                <form id="formCompleta" class="row" action="{{ url('pastores-u-organizacion/admin') }}" method="post"  enctype="multipart/form-data">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">

                 <div class="col-lg-12 formulario__content__top">
                   <div class="col-lg-4 col-12">
                     <div class="filds">
                       <img src="{{ url('storage/'.Auth::user()->avatar) }}" alt="">
                       <input type="file" name="avatar" value="{{ Auth::user()->avatar }}">
                     </div>
                   </div>
                 </div>
                 <div class="col-lg-12 formulario__content__top">
                   <div class="col-lg-4 col-12">
                     <div class="filds">
                       <label for="">Nombre completo</label>
                       <input type="text" class="form-control" name="name" value="{{ Auth::user()->name }}">
                     </div>
                   </div>
                   <div class="col-lg-4 col-12 ">
                     <div class="filds">
                       <label for="">Correo personal</label>
                       <input type="email" name="email" class="form-control" value="{{ Auth::user()->email }}">
                     </div>
                   </div>
                   <div class="col-lg-12 col-12 ">
                     <div class="filds">
                       <label for="">Descripción personal</label>
                       <textarea class="form-control" name="description" ></textarea>
                     </div>
                   </div>
                   <div class="col-lg-4 col-12">
                     <div class="filds">
                       <label for="">Nombre de la organización</label>
                       <input type="text" class="form-control" name="nombre">
                     </div>
                   </div>
                   <div class="col-lg-4 col-12 ">
                     <div class="filds">
                       <label for="">Correo de la organización</label>
                       <input type="email" name="correo" class="form-control">
                     </div>
                   </div>
                   <div class="col-lg-4 col-12">
                     <div class="filds">
                       <label for="">Dirección</label>
                       <input type="text" class="form-control" name="direccion" >
                     </div>
                   </div>
                   <div class="col-lg-4 col-12 ">
                     <div class="filds">
                       <label for="">Horario</label>
                       <input type="text" name="horario" class="form-control">
                     </div>
                   </div>
                   <div class="col-lg-4 col-12 ">
                     <div class="filds">
                       <label for="">Rut</label>
                       <input type="text" name="rut" id="rut" oninput="checkRut(this)"  class="form-control">
                     </div>
                   </div>
                   <div class="col-lg-4 col-12 ">
                     <div class="filds">
                       <label for="">Personalidad Jurídica (opcional)</label>
                       <input type="text" name="personalidad" class="form-control">
                     </div>
                   </div>
                   <div class="col-lg-4 col-12 ">
                     <div class="filds">
                       <label for="">Banco</label>
                       <select class="form-control" name="banco">
                         <option value="" selected>Seleccionar</option>
                         <option value="ABN AMRO">ABN AMRO</option>
                         <option value="Atlas - Citibank">Atlas - Citibank</option>
                         <option value="BancaFacil - Sitio de educación bancaria">BancaFacil - Sitio de educación bancaria</option>
                         <option value="Banco Bice">Banco Bice</option>
                         <option value="Banco Central de Chile">Banco Central de Chile</option>
                         <option value="Banco de Chile">Banco de Chile</option>
                         <option value="Banco de Crédito e Inversiones">Banco de Crédito e Inversiones</option>
                         <option value="Banco del Desarrollo">Banco del Desarrollo</option>
                         <option value="Banco del Desarrollo - Asesoría Financiera">Banco del Desarrollo - Asesoría Financiera</option>
                         <option value="Banco Edwards">Banco Edwards</option>
                         <option value="Banco Falabella">Banco Falabella</option>
                         <option value="Banco Internacional">Banco Internacional</option>
                         <option value="Banco Nova">Banco Nova</option>
                         <option value="Banco Penta">Banco Penta</option>
                         <option value="Banco Santander Santiago">Banco Santander Santiago</option>
                         <option value="Banco Security">Banco Security</option>
                         <option value="BancoEstado">BancoEstado</option>
                         <option value="Citibank N.A. Chile" >Citibank N.A. Chile</option>
                         <option value="BBVA">BBVA</option>
                         <option value="Corpbanca">Corpbanca</option>
                         <option value="Credichile">Credichile</option>
                         <option value="Credit Suisse Consultas y Asesorias Limitada">Credit Suisse Consultas y Asesorias Limitada</option>
                         <option value="Deutsche Bank">Deutsche Bank</option>
                         <option value="ING Bank N.V.">ING Bank N.V.</option>
                         <option value="Redbanc">Redbanc</option>
                         <option value="Santander Banefe">Santander Banefe</option>
                         <option value="Scotiabank Sud Americano">Scotiabank Sud Americano</option>
                         <option value="UBS AG in Santiago de Chile">UBS AG in Santiago de Chile</option>
                       </select>
                     </div>
                   </div>
                   <div class="col-lg-4 col-12 ">
                     <div class="filds">
                       <label for="">Tipo de cuenta</label>
                       <select class="form-control" name="tipo">
                         <option value=""  selected>Seleccionar</option>
                         <option value="Vista" >Vista</option>
                         <option value="Rut">RUT</option>
                         <option value="Corriente">Corriente</option>
                       </select>
                     </div>
                   </div>
                   <div class="col-lg-4 col-12 ">
                     <div class="filds">
                       <label for="">N° de cuenta</label>
                       <input type="text" name="cuenta" class="form-control">
                     </div>
                   </div>
                   <div class="col-lg-12 col-12 ">
                     <div class="filds">
                       <label for="">Describe a la organización</label>
                       <textarea class="form-control" name="descripcion" ></textarea>
                     </div>
                   </div>
                 </div>
                 <div class="col-lg-12 formulario__content__top">

                  <div class="col-lg-6 col-12">
                    <button type="submit" class="btn btn-primary btn-enviar">
                        Enviar
                    </button>
                  </div>

                </div>

                </form>
              </div>
              <div class="alert alert-danger" role="alert">
                  Campo(s) requerido(s)
              </div>
          </div>
      </div>
  </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
function checkRut(rut) {
    // Despejar Puntos
    var valor = rut.value.replace('.','');
    // Despejar Guión
    valor = valor.replace('-','');

    // Aislar Cuerpo y Dígito Verificador
    cuerpo = valor.slice(0,-1);
    dv = valor.slice(-1).toUpperCase();

    // Formatear RUN
    rut.value = cuerpo + '-'+ dv

    // Si no cumple con el mínimo ej. (n.nnn.nnn)
    if(cuerpo.length < 7) { rut.setCustomValidity("RUT Incompleto"); return false;}

    // Calcular Dígito Verificador
    suma = 0;
    multiplo = 2;

    // Para cada dígito del Cuerpo
    for(i=1;i<=cuerpo.length;i++) {

        // Obtener su Producto con el Múltiplo Correspondiente
        index = multiplo * valor.charAt(cuerpo.length - i);

        // Sumar al Contador General
        suma = suma + index;

        // Consolidar Múltiplo dentro del rango [2,7]
        if(multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }

    }

    // Calcular Dígito Verificador en base al Módulo 11
    dvEsperado = 11 - (suma % 11);

    // Casos Especiales (0 y K)
    dv = (dv == 'K')?10:dv;
    dv = (dv == 0)?11:dv;

    // Validar que el Cuerpo coincide con su Dígito Verificador
    if(dvEsperado != dv) { rut.setCustomValidity("RUT Inválido"); return false; }

    // Si todo sale bien, eliminar errores (decretar que es válido)
    rut.setCustomValidity('');
}
</script>
@endsection

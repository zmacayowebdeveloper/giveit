@extends('backend.layout.layout')
@section('contenido')
<div class="right_col margin-content2" role="main">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Sobre mí</div>

                <div class="card-body">
                    <div class="formulario__content" data-aos="fade-up-right">
                      <form id="formPerfil" class="row" action="{{ url('pastores-u-organizacion/mi-perfil') }}" method="post"  enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                       <div class="col-lg-12 formulario__content__top">
                         <div class="col-lg-4 col-12">
                           <div class="filds">
                             <img src="{{ url('storage/'.Auth::user()->avatar) }}" alt="">
                             <input type="file" name="avatar" value="{{ Auth::user()->avatar }}">
                           </div>
                         </div>
                       </div>
                       <div class="col-lg-12 formulario__content__top">
                         <div class="col-lg-6 col-12">
                           <div class="filds">
                             <label for="">Nombre completo</label>
                             <input type="text" class="form-control" name="name" value="{{ Auth::user()->name }}">
                           </div>
                         </div>
                         <div class="col-lg-6 col-12 ">
                           <div class="filds">
                             <label for="">Correo</label>
                             <input type="email" name="email" class="form-control" value="{{ Auth::user()->email }}">
                           </div>
                         </div>
                         <div class="col-lg-6 col-12 ">
                           <div class="filds">
                             <label for="">Contraseña (dejar en blanco si quiere mantener su contraseña actual)</label>
                             <input type="password" name="password" id="pass" class="form-control" >
                           </div>
                         </div>
                         <div class="col-lg-6 col-12 ">
                           <div class="filds">
                             <label for="">Repetir Contraseña</label>
                             <input type="password" name="confirmacion" class="form-control" >
                           </div>
                         </div>
                         <div class="col-lg-12 col-12 ">
                           <div class="filds">
                             <label for="">Descripción </label>
                             <textarea class="form-control" name="description" value="{{ $pastor->descripcion }}">{{ $pastor->descripcion }}</textarea>
                           </div>
                         </div>
                       </div>
                       <div class="col-lg-12 formulario__content__top">
                        <div class="col-lg-6 col-12">
                          <button type="submit" class="btn btn-primary btn-enviar">
                              Enviar
                          </button>
                        </div>
                      </div>
                      </form>
                    </div>
                    <div class="alert alert-danger" role="alert">
                        Campo(s) requerido(s)
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection

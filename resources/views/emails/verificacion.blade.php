<h4>NUEVO REGISTRO DE PASTOR, VERIFIQUE SUS DATOS A CONTINUACIÓN</h4>
<hr>
<p><b>Código:</b> </p>
<p>{{ $data->codigo }}</p>
<p><b>Nombre:</b> </p>
<p>{{ $data->nombre }}</p>
<p><b>Foto de perfil:</b> </p>
<p><img src="{{ url('storage/'.$data->imagen) }}" alt="" style="width:30%"></p>
<p><b>Descripción:</b> </p>
<p>{{ $data->descripcion }}</p>
<p><b>Iglesia:</b> </p>
<p>{{ $data->iglesia->nombre }}</p>
<p><b>Fecha de registro:</b> </p>
<p>{{ $data->created_at }}</p>

<p> <a href="{{ url('verificacion-exitosa/'.$data->id) }}" style="font-size:25px; padding-top:20px; color:black;">Seleccione aquí solo si estos datos son correctos</a> </p>

<link rel="stylesheet" href="{{ url('css/app.css') }}">

<div class="header__top" id="barra">
  <div class="container">
    <div class="row">
      <div class="col-md-12 d-flex justify-content-center" data-aos="zoom-in-up">

      </div>
    </div>
  </div>
</div>
<section class="ofrenda__contenido  m-top" style=" padding: 20px 0;">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="ofrenda__contenido__intro2" style=" background: #FFFFFF;
                                                        border: 1px solid #EBF0F0;
                                                        box-sizing: border-box;
                                                        padding: 30px 30px 30px 30px;
                                                        box-shadow: 0px 4px 27px rgba(1, 15, 38, 0.04);
                                                        text-align:center;">
          <div class="row">
            <div class="col-lg-10 offset-lg-1">
              <div class="ofrenda__contenido__intro2__p3">
                <p style="  font-size: 30px;
                            line-height: 2.35em;
                            font-family: 'Work+Sans:200', 'work sans', sans-serif;
                            color: #546375;">
                  <span>
                    <a href="{{ url('pastores-u-organizacion/admin') }}" target="_blank" style="text-align:center; color:black; text-decoration:none;">Ofir</a>
                  </span>
                </p>
                <h3>Hola <span style="text-transform: capitalize;">{{ $data->name }}</span></h3>
                <p class="split">Bienvenido a la familia Ofir, es necesario que completes tu registro para que tus datos sean validados posteriormente.</p>
                <img src="{{ url('storage/bienvenida.jpg') }}" alt="" style="width:100%">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

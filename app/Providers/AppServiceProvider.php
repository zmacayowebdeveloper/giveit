<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $contacteno = \App\Contacteno::first();
        $login = \App\Bloque::where('seccion', 'login')->first();
        $registro = \App\Bloque::where('seccion', 'registro')->first();
        $email = \App\Bloque::where('seccion', 'email')->first();
        $nueva = \App\Bloque::where('seccion', 'nueva')->first();


        view()->share('contacteno', $contacteno);
        view()->share('login', $login);
        view()->share('registro', $registro);
        view()->share('email', $email);
        view()->share('nueva', $nueva);

    }
}

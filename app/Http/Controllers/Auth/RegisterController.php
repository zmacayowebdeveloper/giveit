<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Pastor;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Mail;
use App\Helpers\Frontend\EnviosCorreo as HelperCorreo;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = 'pastores-u-organizacion/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'contraseña' => ['required', 'string', 'min:6', 'confirmed'],
        ]);

    }

    protected function create(array $data)
    {
        // return User::create([
        //     'name' => $data['name'],
        //     'email' => $data['email'],
        //     'password' => Hash::make($data['password']),
        // ]);

        $user = User::create([
             'name' => $data['name'],
             'email' => $data['email'],
             'role_id' => 3,
             'password' => Hash::make($data['contraseña']),
         ]);

         Pastor::create([
              'codigo' => 'OFIR-00'.$user->id,
              'nombre' => $user->name,
              'imagen' => $user->avatar,
              'user_id' => $user->id,
          ]);

         $subject = 'Bienvenido';
         HelperCorreo::sendMail('emails.bienvenida', $user, $user->email, $subject);

         return $user;
         // return response()->json(array('msg' => "creado"), 201);

    }
}

<?php

namespace App\Http\Controllers;

use App\Pastor;
use App\Iglesia;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Validator;
use Mail;
use Auth;
use Illuminate\Support\Facades\Input;

use App\Helpers\Frontend\EnviosCorreo as HelperCorreo;

class PastorController extends Controller
{
    public function index()
    {

        return view('frontend.pastores.index');

    }

    public function login(Request $request)
    {

      $this->validate($request, [
          'email' => 'required', 'contraseña' => 'required'
      ]);

      $userData = \App\User::where('email',request('email'))->first();
       if ($userData && \Hash::check(request('contraseña'), $userData->password))
       {
         auth()->loginUsingId($userData->id);
         return redirect('pastores-u-organizacion/admin');
       }else{
         return redirect('pastores-u-organizacion');
       }


    }

    public function logout(Request $request){
      Auth::logout();
      return redirect('pastores-u-organizacion');
    }

    public function admin()
    {
      $user = Auth::user();

      if($user && $user->role_id == '3'){
        $pastor = Pastor::where('user_id', $user->id)->first();

        if($pastor->verificacion == 0){
          return view('backend.bienvenida');
        }else{
          return redirect('pastores-u-organizacion/estadisticas');
        }
      }else{
        return back();
      }
    }

    public function dashboard()
    {
      $user = Auth::user();
      $pastor = Pastor::where('user_id', $user->id)->first();


      if($pastor && $pastor->verificacion != 0 && $user && $user->role_id == '3'){

        $total = $pastor->visitas;

        $hoy = date('Y-m-d');
        $visitas = \App\Visita::where('pastor_id', $pastor->id)->where('fecha', $hoy)->get();
        $contador = count($visitas);
        return view('backend.estadisticas', compact('total', 'contador', 'hoy'));
      }else{
        return back();
      }
    }

    public function perfil()
    {
      $user = Auth::user();
      $pastor = Pastor::where('user_id', $user->id)->first();

      if($pastor->verificacion != 0 && $user && $user->role_id == '3'){
        return view('backend.perfil', compact('pastor'));
      }else{
        return back();
      }
    }

    public function iglesia()
    {
      $user = Auth::user();
      $pastor = Pastor::where('user_id', $user->id)->first();
      $iglesia = Iglesia::find($pastor->iglesia_id);

      if($iglesia && $user->role_id == '3'){
          return view('backend.iglesia', compact('iglesia'));
      }else{
        return back();
      }
    }


    public function registro(Request $request)
    {

        $user = Auth::user();
        $pastor = Pastor::where('user_id', $user->id)->first();

        if(Input::file('avatar')){
          $file = Input::file('avatar');
          $imagen= time (). '.'. $file->getClientOriginalName();
          $file->move(storage_path().'/app/public/users', $imagen);
          $user->avatar = 'users/'.$imagen;
          $pastor->imagen = 'users/'.$imagen;
        }


        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->save();

        $church = new Iglesia;
        $church->nombre = $request->input('nombre');
        $church->descripcion = $request->input('descripcion');
        $church->correo = $request->input('correo');
        $church->direccion = $request->input('direccion');
        $church->horario = $request->input('horario');
        $church->rut = $request->input('rut');
        $church->personalidad = $request->input('personalidad');
        $church->banco = $request->input('banco');
        $church->tipo = $request->input('tipo');
        $church->cuenta = $request->input('cuenta');
        $church->save();

        $pastor->nombre = $request->input('name');
        $pastor->descripcion = $request->input('description');
        $pastor->iglesia_id = $church->id;
        $pastor->save();

        // $correo = 'ofir@rayoingenieria.cl';
        $correo = 'curibe@rayoingenieria.cl';
        $subject = 'Validar registro';
        HelperCorreo::sendMail('emails.verificacion', $pastor, $correo, $subject);

        return response()->json(array('msg' => "completado"), 201);
    }

    public function store(Request $request)
    {
        $user = Auth::user();
        $pastor = Pastor::where('user_id', $user->id)->first();

        if(Input::file('avatar')){
          $file = Input::file('avatar');
          $imagen= time (). '.'. $file->getClientOriginalName();
          $file->move(storage_path().'/app/public/users', $imagen);
          $user->avatar = 'users/'.$imagen;
          $pastor->imagen = 'users/'.$imagen;
        }

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        if($request->input('password'))
          $user->password = bcrypt($request->input('password'));
        $user->save();

        $pastor->nombre = $request->input('name');
        $pastor->descripcion = $request->input('description');
        $pastor->save();

        return response()->json(array('msg' => "actualizado"), 201);
    }

    public function storeiglesia(Request $request)
    {

      $user = Auth::user();

      $pastor = \App\Pastor::where('user_id', $user->id)->first();
      $iglesia = \App\Iglesia::findOrFail($pastor->iglesia_id);

      $iglesia->nombre = $request->input('nombre');
      $iglesia->descripcion = $request->input('descripcion');
      $iglesia->correo = $request->input('correo');
      $iglesia->direccion = $request->input('direccion');
      $iglesia->horario = $request->input('horario');
      $iglesia->rut = $request->input('rut');
      $iglesia->personalidad = $request->input('personalidad');
      $iglesia->banco = $request->input('banco');
      $iglesia->tipo = $request->input('tipo');
      $iglesia->cuenta = $request->input('cuenta');
      $iglesia->save();

      return response()->json(array('msg' => "actualizado"), 201);
    }

   public function verificacion($id){
     $pastor = Pastor::find($id);
     $pastor->verificacion = 1;
     $pastor->save();

     $user = User::find($pastor->user_id);

     $subject = 'VERIFICACIÓN EXITOSA';
     HelperCorreo::sendMail('emails.verificacionexitosa', $user, $user->email, $subject);

     return view('frontend.verificacion');
   }
}

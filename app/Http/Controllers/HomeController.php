<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pastor;
use App\Iglesia;
use App\Visita;
use Auth;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index()
    {
        $banner = \App\Bloque::where('seccion', 'banner')->first();
        $nosotros = \App\Bloque::where('seccion', 'nosotros')->first();
        $causa = \App\Bloque::where('seccion', 'causas')->first();
        $causa_2 = \App\Bloque::where('seccion', 'causas_2')->first();
        $causas = \App\Causa::all();
        return view('frontend.inicio.index', compact('banner', 'nosotros', 'causa', 'causa_2', 'causas'));
    }

    public function ofrenda()
    {
      // return view('home');
        $pastores = Pastor::where('verificacion', 1)->paginate(8);
        $var = $pastores->currentPage();
        $contador = $pastores->lastPage();
        $mensaje = null;

        return view('frontend.miofrenda.index', compact('pastores', 'var', 'contador', 'mensaje'));
    }

    public function search(Request $request){
       if($request->ajax()){
           $name = $request->get('texto');

           $pastores = Pastor::searchlive($name)->where('verificacion', 1)->paginate(8);
           $iglesias = null;
           if(count($pastores) == 0)
            $iglesias = Iglesia::searchlive($name)->get();

           if($iglesias){
             foreach ($iglesias as $c)
              {
                 $pastores = Pastor::where('iglesia_id', $c->id)->where('verificacion', 1)->paginate(1);
                 // $pastores[] = array(
                 //       $pastor
                 //     // "nombre" => $pastor->nombre,
                 //     // "Correo" => $pastor->correo,
                 //     // "Teléfono" => $pastor->telefono,
                 //     // "Empresa" => $pastor->empresa,
                 //     // "País" => $pastor->pais,
                 //     // "Ciudad" => $pastor->ciudad,
                 //     // "Mensaje" => $pastor->mensaje,
                 //  );
              }
           }


           $var = $pastores->currentPage();
           $contador = $pastores->lastPage();
           $mensaje = null;

           if(count($pastores) == 0)
              $mensaje = 'Lo sentimos, no existen resultados bajo el criterio de búsqueda seleccionado.';


           if($contador == 0){
             $var = $pastores->lastPage();
           }

           return response()->json(view('frontend.miofrenda.partials.pastores', compact( 'pastores', 'var', 'contador', 'mensaje'))->render());
       }
     }

    public function gracias()
    {
      // return view('home');
        return view('frontend.gracias.index');
    }

    public function visitas($inicial, $final){

      $arreglo = [];
      $resultado = [];
      $j = 0;


      for($i=$inicial; $i<=$final; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){

            $fecha = substr($i, 0, 10);
            $numeroDia = date('d', strtotime($fecha));
            $dia = date('l', strtotime($fecha));
            $mes = date('F', strtotime($fecha));
            $anio = date('Y', strtotime($fecha));
            $dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
            $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
            $nombredia = str_replace($dias_EN, $dias_ES, $dia);
            $meses_ES = array("Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic");
            $meses_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
            $nombreMes = str_replace($meses_EN, $meses_ES, $mes);

            $arreglo[$j] = $numeroDia." ".$nombreMes;

            $user = Auth::user();
            $pastor = Pastor::where('user_id', $user->id)->first();
            $visitas = \App\Visita::where('pastor_id', $pastor->id)->where('fecha', $i)->get();

            $resultado[$j] = count($visitas);

            $j= $j+1;

      }


      return response()->json(['resultado' => $resultado, 'array' => $arreglo, 'cantidad' => $j]);
    }

    public function show($id)
    {
      $pastor = Pastor::find($id);
      $hoy = date('Y-m-d');

      // dd($hoy);

      $pastor->visitas = $pastor->visitas + 1;
      $pastor->save();

      Visita::create([
           'fecha' => $hoy,
           'pastor_id' => $id,
       ]);

      return view('frontend.miofrenda.informacion', compact('pastor'));

    }

    public function contacto(Request $request){

        $this->validate($request, [
            'nombre' => 'required|string',
            'email' => 'required|email',
            'telefono' => 'required|integer|min:8',
            'mensaje' => 'required',
        ]);


        $contacto= new \App\Contacto;
        $contacto->nombre = $request->input('nombre');
        $contacto->email = $request->input('email');
        $contacto->direccion = $request->input('direccion');
        $contacto->telefono = $request->input('telefono');
        $contacto->asunto = $request->input('asunto');
        $contacto->mensaje = $request->input('mensaje');
        $contacto->save();


        // $correo = '';
        //
        // $emails = $correo;
        // $subject = 'Nuevo contacto desde la web Give-it: ' ;
        // HelperCorreo::sendMailContacto('emails.contacto', $contacto, $emails, $subject);


        return response()->json(array('msg' => "creado"), 201);
    }

    public function suscripcion(Request $request){

          $this->validate($request, [
              'email' => 'required|email',
          ]);

          $contacto= new \App\Suscriptor;
          $contacto->correo = $request->input('email');
          $contacto->save();

        //
        // $correo = '';
        //
        // $emails = $correo;
        // $subject = 'Nuevo suscriptor desde la web Give-it: ' ;
        // HelperCorreo::sendMailContacto('emails.suscriptor', $contacto, $emails, $subject);


      return response()->json(array('msg' => "creado"), 201);
    }

    public function export(Request $request)
    {

      $inicio= $request->fecha_inicio;
      $fin= $request->fecha_fin;

       Excel::create('Contactos', function($excel) use($inicio, $fin) {

             $excel->sheet('Contactos', function($sheet) use($inicio, $fin) {

              if($inicio == null && $fin == null){
               $contactos = DB::table('contactos')
                           ->select('contactos.*', DB::raw('DATE_SUB(contactos.created_at, INTERVAL 5 HOUR) as created_at'))
                           ->orderBy('contactos.created_at','desc')
                           ->get();
              }elseif($fin == null){
                $contactos = DB::table('contactos')
                            ->select('contactos.*', DB::raw('DATE_SUB(contactos.created_at, INTERVAL 5 HOUR) as created_at'))
                            ->orderBy('contactos.created_at','desc')
                            ->whereDate('contactos.created_at', $inicio)
                            ->get();
              }else{
                $contactos = DB::table('contactos')
                            ->select('contactos.*', DB::raw('DATE_SUB(contactos.created_at, INTERVAL 5 HOUR) as created_at'))
                            ->orderBy('contactos.created_at','desc')
                            ->whereBetween('contactos.created_at',[$inicio , $fin] )
                            ->get();
              }


            $exist= count($contactos);
            if($exist == 0){
              $data[] = array(
                "Nombres y Apellidos" => '',
                "Correo" => '',
                "Teléfono" => '',
                "Dirección" => '',
                "Asunto" => '',
                "Mensaje" => '',
                "Fecha de registro" => '',
               );
            }else{

             foreach ($contactos as $c)
              {
                 $data[] = array(
                     "Nombres y Apellidos" => $c->nombre,
                     "Correo" => $c->email,
                     "Teléfono" => $c->telefono,
                     "Dirección" => $c->direccion,
                     "Asunto" => $c->asunto,
                     "Mensaje" => $c->mensaje,
                     "Fecha de registro" => $c->created_at,
                  );
              }
            }


            $sheet->fromArray($data);

             });
         })->export('xlsx');
    }

    public function exportSuscripcion(Request $request)
    {

      $inicio= $request->fecha_inicio;
      $fin= $request->fecha_fin;

       Excel::create('Suscriptores', function($excel) use($inicio, $fin) {

             $excel->sheet('Suscriptores', function($sheet) use($inicio, $fin) {

              if($inicio == null && $fin == null){
               $suscriptors = DB::table('suscriptors')
                           ->select('suscriptors.*', DB::raw('DATE_SUB(suscriptors.created_at, INTERVAL 5 HOUR) as created_at'))
                           ->orderBy('suscriptors.created_at','desc')
                           ->get();
              }elseif($fin == null){
                $suscriptors = DB::table('suscriptors')
                            ->select('suscriptors.*', DB::raw('DATE_SUB(suscriptors.created_at, INTERVAL 5 HOUR) as created_at'))
                            ->orderBy('suscriptors.created_at','desc')
                            ->whereDate('suscriptors.created_at', $inicio  )
                            ->get();
              }else{
                $suscriptors = DB::table('suscriptors')
                            ->select('suscriptors.*', DB::raw('DATE_SUB(suscriptors.created_at, INTERVAL 5 HOUR) as created_at'))
                            ->orderBy('suscriptors.created_at','desc')
                            ->whereBetween('suscriptors.created_at',[$inicio , $fin] )
                            ->get();
              }


            $exist= count($suscriptors);
            if($exist == 0){
              $data[] = array(
                "Correo" => '',
                "Fecha de registro" => '',
               );
            }else{

             foreach ($suscriptors as $c)
              {
                 $data[] = array(
                     "Correo" => $c->correo,
                     "Fecha de registro" => $c->created_at,
                  );
              }
            }


            $sheet->fromArray($data);

             });
         })->export('xlsx');
    }

    public function exportPastores(Request $request)
    {

      $inicio= $request->fecha_inicio;
      $fin= $request->fecha_fin;

       Excel::create('Pastores', function($excel) use($inicio, $fin) {

             $excel->sheet('Pastores', function($sheet) use($inicio, $fin) {

              if($inicio == null && $fin == null){
               $pastors = DB::table('pastors')
                           ->select('pastors.*', DB::raw('DATE_SUB(pastors.created_at, INTERVAL 5 HOUR) as created_at'))
                           ->orderBy('pastors.created_at','desc')
                           ->get();
              }elseif($fin == null){
                $pastors = DB::table('pastors')
                            ->select('pastors.*', DB::raw('DATE_SUB(pastors.created_at, INTERVAL 5 HOUR) as created_at'))
                            ->orderBy('pastors.created_at','desc')
                            ->whereDate('pastors.created_at', $inicio)
                            ->get();
              }else{
                $pastors = DB::table('pastors')
                            ->select('pastors.*', DB::raw('DATE_SUB(pastors.created_at, INTERVAL 5 HOUR) as created_at'))
                            ->orderBy('pastors.created_at','desc')
                            ->whereBetween('pastors.created_at',[$inicio , $fin] )
                            ->get();
              }


            $exist= count($pastors);
            if($exist == 0){
              $data[] = array(
                "Código" => '',
                "Nombres y Apellidos" => '',
                "Verificado" => '',
                "Fecha de registro" => '',
                "Total de Visitas" => '',
               );
            }else{

             foreach ($pastors as $c)
              {
                  if($c->verificacion == 1){
                    $verificado = 'Verificado';
                  }else{
                    $verificado = 'No Verificado';
                  }
                 $data[] = array(
                     "Código" => $c->codigo,
                     "Nombres y Apellidos" => $c->nombre,
                     "Verificado" => $verificado,
                     "Fecha de registro" => $c->created_at,
                     "Total de Visitas" => $c->visitas,
                  );
              }
            }


            $sheet->fromArray($data);

             });
         })->export('xlsx');
    }

}

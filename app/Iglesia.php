<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Iglesia extends Model
{
  public function pastor()
  {
  return $this->hasOne('App\Pastor', 'iglesia_id');
  }

  public function scopeSearchlive($query,$name){
    if($name)
    return  $query->where('nombre','like',"%$name%");
  }
}

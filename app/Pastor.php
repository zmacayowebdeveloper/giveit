<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pastor extends Model
{
  protected $fillable = [
      'codigo', 'nombre', 'imagen', 'user_id'
  ];

  public function iglesia()
  {
    return $this->belongsTo('App\Iglesia');
  }

  public function scopeSearchlive($query,$name){
    if($name)
    return  $query->where('nombre','like',"%$name%")->orWhere('codigo','like',"%$name%");
  }
}

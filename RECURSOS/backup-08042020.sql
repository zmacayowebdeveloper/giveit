-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 08-04-2020 a las 16:09:39
-- Versión del servidor: 5.7.24
-- Versión de PHP: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `giveit`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bloques`
--

CREATE TABLE `bloques` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci,
  `descripcion_2` text COLLATE utf8mb4_unicode_ci,
  `imagen` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seccion` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `bloques`
--

INSERT INTO `bloques` (`id`, `titulo`, `descripcion`, `descripcion_2`, `imagen`, `seccion`, `created_at`, `updated_at`) VALUES
(1, 'Ofir', 'la belleza de dar a la obra de Dios', NULL, 'bloques\\April2020\\xpB7kIiNZi5UiE3u0gHI.jpg', 'banner', '2020-04-08 20:04:54', '2020-04-08 20:04:54'),
(2, 'Lo que hacemos', 'Una mano que ayuda', 'Somos una plataforma online para vincular Pastores Cristianos y fieles en el acto de ofrendar para la obra de Dios, aportando a la continuidad de la obra social de la iglesia que no se detiene.', 'bloques\\April2020\\A7p0atFdfRoBrFmwnwvM.jpeg', 'nosotros', '2020-04-08 20:05:53', '2020-04-08 20:05:53'),
(3, 'Nuestras causas', 'Todo comienza con la voluntad de hacer la diferencia', NULL, NULL, 'causas', '2020-04-08 20:06:27', '2020-04-08 20:06:27'),
(4, 'JUNTOS  SOMOS MAS QUE VENCEDORES EN CRISTO JESÚS', 'DIOS BENDICE AL QUE OFRENDA CON ALEGRIA', NULL, 'bloques\\April2020\\ngPSOa70yeyLVZbzkkSL.jpg', 'causas_2', '2020-04-08 20:07:19', '2020-04-08 20:07:19'),
(5, 'Inicia Sesión', NULL, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. eiusmod tempor incididunt ut labore et dolore magna aliqu', 'bloques\\April2020\\CUJXz4nVQk4DRW3mDwoK.jpg', 'login', '2020-04-08 20:44:42', '2020-04-08 20:44:42'),
(6, 'Forma parte de esta familia', NULL, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. eiusmod tempor incididunt ut labore et dolore magna aliqu', 'bloques\\April2020\\GgIcyVzx6BRXuVRaaQlg.jpg', 'registro', '2020-04-08 20:45:18', '2020-04-08 20:45:18'),
(7, 'Recupera tu contraseña', NULL, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. eiusmod tempor incididunt ut labore et dolore magna aliqu', 'bloques\\April2020\\dIZLh1trrZKymj0p49ae.jpg', 'email', '2020-04-08 20:45:52', '2020-04-08 20:45:52'),
(8, 'Ingresa tu nueva contraseña', NULL, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. eiusmod tempor incididunt ut labore et dolore magna aliqu', 'bloques\\April2020\\9nkBPtMwCC2vhU4fHY2Z.jpg', 'nueva', '2020-04-08 20:46:20', '2020-04-08 20:46:20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `causas`
--

CREATE TABLE `causas` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci,
  `imagen` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `causas`
--

INSERT INTO `causas` (`id`, `titulo`, `descripcion`, `imagen`, `created_at`, `updated_at`) VALUES
(1, 'Trabajo social y colaborativo con nuevas generaciones', 'Ayudar al prójimo', 'causas\\April2020\\oo7OyJUoJqSe4sQAyoRx.jpg', '2020-04-08 20:08:02', '2020-04-08 20:08:02'),
(2, 'Obra social  de la iglesia hacia fuera y dentro de nuestras congregaciones', 'No más desigualdad', 'causas\\April2020\\J64XUsanDAqq4s4tu2t7.jpeg', '2020-04-08 20:08:57', '2020-04-08 20:08:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contactenos`
--

CREATE TABLE `contactenos` (
  `id` int(10) UNSIGNED NOT NULL,
  `direccion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `correo` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `contactenos`
--

INSERT INTO `contactenos` (`id`, `direccion`, `correo`, `telefono`, `facebook`, `twitter`, `linkedin`, `created_at`, `updated_at`) VALUES
(1, 'Providencia, Santiago de Chile', 'contacto@ofir.cl', '982303304', '#', '#', '#', '2020-04-08 20:09:56', '2020-04-08 20:09:56');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contactos`
--

CREATE TABLE `contactos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `asunto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mensaje` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `contactos`
--

INSERT INTO `contactos` (`id`, `nombre`, `email`, `telefono`, `direccion`, `asunto`, `mensaje`, `created_at`, `updated_at`) VALUES
(1, 'Prueba', 'prueba@prueba.com', '3214878', 'i kjfgkfdjkg fd|', 'kjkj jkjkj kkjk', 'jkkfkdjs gfkgkd', '2020-04-08 18:15:31', '2020-04-08 18:15:31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'locale', 'text', 'Locale', 0, 1, 1, 1, 1, 0, NULL, 12),
(12, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(13, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(14, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(15, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(16, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(17, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(18, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(19, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(20, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(21, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(22, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(32, 6, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(33, 6, 'nombre', 'text', 'Nombre', 1, 1, 1, 1, 1, 1, '{}', 2),
(34, 6, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '{}', 3),
(35, 6, 'telefono', 'text', 'Teléfono', 1, 1, 1, 1, 1, 1, '{}', 4),
(36, 6, 'direccion', 'text', 'Dirección', 0, 0, 1, 1, 1, 1, '{}', 5),
(37, 6, 'asunto', 'text', 'Asunto', 0, 1, 1, 1, 1, 1, '{}', 6),
(38, 6, 'mensaje', 'text_area', 'Mensaje', 1, 0, 1, 1, 1, 1, '{}', 7),
(39, 6, 'created_at', 'timestamp', 'Fecha de registro', 0, 0, 1, 0, 0, 0, '{}', 8),
(40, 6, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(41, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(42, 7, 'correo', 'text', 'Correo', 1, 1, 1, 1, 1, 1, '{}', 2),
(43, 7, 'created_at', 'timestamp', 'Fecha de registro', 0, 0, 1, 0, 0, 0, '{}', 3),
(44, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(45, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(46, 8, 'nombre', 'text', 'Nombre', 1, 1, 1, 1, 1, 1, '{}', 4),
(47, 8, 'codigo', 'text', 'Código', 1, 1, 1, 1, 1, 1, '{}', 5),
(48, 8, 'descripcion', 'text_area', 'Descripción', 0, 0, 1, 1, 1, 1, '{}', 6),
(49, 8, 'verificacion', 'select_dropdown', 'Verificado', 1, 1, 1, 1, 1, 1, '{\"default\":\"0\",\"options\":{\"0\":\"No verificado\",\"1\":\"Verificado\"}}', 7),
(50, 8, 'imagen', 'image', 'Imagen', 0, 1, 1, 1, 1, 1, '{}', 8),
(51, 8, 'user_id', 'text', 'User Id', 1, 0, 0, 0, 0, 0, '{}', 2),
(52, 8, 'iglesia_id', 'text', 'Iglesia Id', 0, 0, 1, 1, 1, 1, '{}', 3),
(53, 8, 'visitas', 'text', 'Visitas', 1, 0, 0, 0, 0, 0, '{}', 9),
(54, 8, 'created_at', 'timestamp', 'Fecha de registro', 0, 0, 1, 0, 0, 0, '{}', 10),
(55, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 11),
(56, 8, 'pastor_belongsto_iglesia_relationship', 'relationship', 'Iglesia', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Iglesia\",\"table\":\"iglesias\",\"type\":\"belongsTo\",\"column\":\"iglesia_id\",\"key\":\"id\",\"label\":\"nombre\",\"pivot_table\":\"contactos\",\"pivot\":\"0\",\"taggable\":\"0\"}', 12),
(80, 12, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(81, 12, 'titulo', 'text', 'Titulo', 1, 1, 1, 1, 1, 1, '{}', 2),
(82, 12, 'descripcion', 'text', 'Descripción', 0, 1, 1, 1, 1, 1, '{}', 3),
(83, 12, 'descripcion_2', 'text_area', 'Descripción 2', 0, 1, 1, 1, 1, 1, '{}', 4),
(84, 12, 'imagen', 'image', 'Imagen', 0, 1, 1, 1, 1, 1, '{}', 5),
(85, 12, 'seccion', 'select_dropdown', 'Seccion', 0, 1, 1, 1, 1, 1, '{\"default\":\"banner\",\"options\":{\"banner\":\"Banner\",\"nosotros\":\"Nosotros\",\"causas\":\"Causas sin imagen\",\"causas_2\":\"Causas con imagen\",\"registro\":\"Registro pastor\",\"login\":\"Login pastor\",\"email\":\"Email para recuperar contrase\\u00f1a\",\"nueva\":\"Nueva contrase\\u00f1a\"}}', 6),
(86, 12, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 7),
(87, 12, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(88, 13, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(89, 13, 'titulo', 'text', 'Título', 1, 1, 1, 1, 1, 1, '{}', 2),
(90, 13, 'descripcion', 'text', 'Descripción', 0, 1, 1, 1, 1, 1, '{}', 3),
(91, 13, 'imagen', 'image', 'Imagen', 0, 1, 1, 1, 1, 1, '{}', 4),
(92, 13, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 5),
(93, 13, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(94, 14, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(95, 14, 'direccion', 'text', 'Dirección', 1, 1, 1, 1, 1, 1, '{}', 2),
(96, 14, 'correo', 'text', 'Correo', 1, 1, 1, 1, 1, 1, '{}', 3),
(97, 14, 'telefono', 'text', 'Teléfono', 1, 1, 1, 1, 1, 1, '{}', 4),
(98, 14, 'facebook', 'text', 'Facebook', 0, 1, 1, 1, 1, 1, '{}', 5),
(99, 14, 'twitter', 'text', 'Twitter', 0, 1, 1, 1, 1, 1, '{}', 6),
(100, 14, 'linkedin', 'text', 'Linkedin', 0, 1, 1, 1, 1, 1, '{}', 7),
(101, 14, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 8),
(102, 14, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', '', '', 1, 0, NULL, '2020-03-29 19:58:37', '2020-03-29 19:58:37'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2020-03-29 19:58:37', '2020-03-29 19:58:37'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2020-03-29 19:58:37', '2020-03-29 19:58:37'),
(6, 'contactos', 'contactos', 'Contacto', 'Contactos', NULL, 'App\\Contacto', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-04-08 18:04:08', '2020-04-08 18:04:08'),
(7, 'suscriptors', 'suscripciones-al-boletin', 'Suscripción al boletín', 'Suscripciones al boletín', NULL, 'App\\Suscriptor', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-04-08 18:13:18', '2020-04-08 18:13:18'),
(8, 'pastors', 'pastores', 'Pastor', 'Pastores', NULL, 'App\\Pastor', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-04-08 19:00:47', '2020-04-08 19:02:53'),
(12, 'bloques', 'bloques', 'Bloque', 'Bloques', NULL, 'App\\Bloque', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-04-08 20:01:47', '2020-04-08 20:50:48'),
(13, 'causas', 'causas', 'Causa', 'Causas', NULL, 'App\\Causa', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-04-08 20:02:15', '2020-04-08 20:02:15'),
(14, 'contactenos', 'contactenos', 'Contacto', 'Contáctenos', NULL, 'App\\Contacteno', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-04-08 20:02:49', '2020-04-08 20:02:49');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `iglesias`
--

CREATE TABLE `iglesias` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `correo` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rut` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `personalidad` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tipo` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cuenta` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `provincia` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ciudad` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direccion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `horario` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imagen` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `iglesias`
--

INSERT INTO `iglesias` (`id`, `nombre`, `correo`, `rut`, `personalidad`, `tipo`, `cuenta`, `descripcion`, `provincia`, `ciudad`, `direccion`, `horario`, `imagen`, `created_at`, `updated_at`) VALUES
(3, 'La Esperanza', 'laesperanza@gmail.com', '12345678-K', NULL, 'visa', '11345666', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', NULL, NULL, 'Av. San Carlos', 'Sáb 9 am', NULL, '2020-04-06 21:10:03', '2020-04-06 21:37:07'),
(4, 'La Esperanza', 'laesperanza@gmail.com', '12345678-K', NULL, 'ahorro', '4356476586', 'erfes er we r', NULL, NULL, 'Av. Evitamiento 3636', 'Sáb 9 am', NULL, '2020-04-06 22:07:08', '2020-04-06 22:07:08'),
(5, 'La Esperanza', 'laesperanza@gmail.com', '12345678-9', 'fgfdgfd fg', 'ahorro', '2355476765', 'rewrtwv rtretre twtre', NULL, NULL, 'Av. Evitamiento 3636', 'Sáb 9 am', NULL, '2020-04-06 22:10:07', '2020-04-06 22:10:07'),
(6, 'La Esperanza', 'laesperanza@gmail.com', '12345679-9', NULL, 'corriente', '8776897979', 'dfgf fg trhjshjhkf khj', NULL, NULL, 'skdfkkk', 'Sáb 9 am', NULL, '2020-04-06 22:12:40', '2020-04-06 22:12:40'),
(7, 'La vida', 'lavida@gmail.com', '12345587-K', NULL, 'visa', '4895 - 000 - 1723', 'cfdskjgkkgfskdgjkfd', NULL, NULL, 'Av. Evitamiento', 'Sáb 9 am Dom 10 am Jue 7pm', NULL, '2020-04-08 19:46:10', '2020-04-08 19:46:10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2020-03-29 19:58:38', '2020-03-29 19:58:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2020-03-29 19:58:38', '2020-03-29 19:58:38', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 5, '2020-03-29 19:58:38', '2020-03-29 19:58:38', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2020-03-29 19:58:38', '2020-03-29 19:58:38', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2020-03-29 19:58:38', '2020-03-29 19:58:38', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 9, '2020-03-29 19:58:38', '2020-03-29 19:58:38', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 10, '2020-03-29 19:58:38', '2020-03-29 19:58:38', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 11, '2020-03-29 19:58:38', '2020-03-29 19:58:38', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 12, '2020-03-29 19:58:38', '2020-03-29 19:58:38', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 13, '2020-03-29 19:58:38', '2020-03-29 19:58:38', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 14, '2020-03-29 19:58:38', '2020-03-29 19:58:38', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 13, '2020-03-29 19:58:41', '2020-03-29 19:58:41', 'voyager.hooks', NULL),
(13, 1, 'Contactos', '', '_self', NULL, NULL, NULL, 15, '2020-04-08 18:04:08', '2020-04-08 18:04:08', 'voyager.contactos.index', NULL),
(14, 1, 'Suscripciones al boletín', '', '_self', NULL, NULL, NULL, 16, '2020-04-08 18:13:19', '2020-04-08 18:13:19', 'voyager.suscripciones-al-boletin.index', NULL),
(15, 1, 'Pastores', '', '_self', NULL, NULL, NULL, 17, '2020-04-08 19:00:47', '2020-04-08 19:00:47', 'voyager.pastores.index', NULL),
(19, 1, 'Bloques', '', '_self', NULL, NULL, NULL, 18, '2020-04-08 20:01:47', '2020-04-08 20:01:47', 'voyager.bloques.index', NULL),
(20, 1, 'Causas', '', '_self', NULL, NULL, NULL, 19, '2020-04-08 20:02:15', '2020-04-08 20:02:15', 'voyager.causas.index', NULL),
(21, 1, 'Contáctenos', '', '_self', NULL, NULL, NULL, 20, '2020-04-08 20:02:49', '2020-04-08 20:02:49', 'voyager.contactenos.index', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 2),
(4, '2016_01_01_000000_create_data_types_table', 2),
(5, '2016_05_19_173453_create_menu_table', 2),
(6, '2016_10_21_190000_create_roles_table', 2),
(7, '2016_10_21_190000_create_settings_table', 2),
(8, '2016_11_30_135954_create_permission_table', 2),
(9, '2016_11_30_141208_create_permission_role_table', 2),
(10, '2016_12_26_201236_data_types__add__server_side', 2),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 2),
(12, '2017_01_14_005015_create_translations_table', 2),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 2),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 2),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 2),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 2),
(17, '2017_08_05_000000_add_group_to_settings_table', 2),
(18, '2017_11_26_013050_add_user_role_relationship', 2),
(19, '2017_11_26_015000_create_user_roles_table', 2),
(20, '2018_03_11_000000_add_user_settings', 2),
(21, '2018_03_14_000000_add_details_to_data_types_table', 2),
(22, '2018_03_16_000000_make_settings_value_nullable', 2),
(23, '2020_03_29_184318_create_iglesias_table', 3),
(24, '2020_03_30_233203_create_contactos_table', 3),
(25, '2020_03_30_233339_create_suscriptors_table', 3),
(26, '2020_03_31_170158_create_pastors_table', 3),
(27, '2020_04_05_191219_add_verificacion_to_pastors_table', 4),
(28, '2020_04_05_195324_add_correo_to_iglesias_table', 4),
(29, '2020_04_07_135709_add_visitas_to_pastors_table', 5),
(30, '2020_04_07_140418_create_visitas_table', 5),
(34, '2020_04_08_143422_create_bloques_table', 6),
(35, '2020_04_08_143558_create_causas_table', 6),
(36, '2020_04_08_143615_create_contactenos_table', 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('admin@giveit.com', '$2y$10$RiQck0AcoFEyFYReopX47eToB/XTNFwXNJrrzkWklazcsuz34rnKK', '2020-03-29 23:05:45'),
('zmacayo@gmail.com', '$2y$10$vUA9um2Fj5OiD27U5MNLh.NHSNdsPL/I9qKNcDFeR7349AT.AIOOa', '2020-04-06 22:04:20'),
('carlos_1@gmail.com', '$2y$10$jUaLRZPvUu1.RL.duGaKhuHVDfsjeRZfxGx5FP6jdWMmJuRlIEkMa', '2020-04-08 20:53:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pastors`
--

CREATE TABLE `pastors` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `codigo` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci,
  `verificacion` tinyint(1) NOT NULL DEFAULT '0',
  `imagen` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `iglesia_id` int(10) UNSIGNED DEFAULT NULL,
  `visitas` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `pastors`
--

INSERT INTO `pastors` (`id`, `nombre`, `codigo`, `descripcion`, `verificacion`, `imagen`, `user_id`, `iglesia_id`, `visitas`, `created_at`, `updated_at`) VALUES
(1, 'Carlos fernandez', 'giv-003', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\nqui officia deserunt mollit anim id est laborum.\r\nqui officia deserunt mollit anim id est laborum.', 1, 'users/1586300929.default.png', 3, 3, 10, '2020-04-03 22:12:59', '2020-04-08 17:35:57'),
(2, 'Carlos fer', 'giv-004', 'srewr', 1, 'users/default.png', 4, 4, 1, '2020-04-06 22:06:27', '2020-04-08 17:36:49'),
(3, 'Luis Reyes', 'giv-005', 'dsfdsf', 1, 'iglesia.jpg', 5, 5, 2, '2020-04-06 22:09:34', '2020-04-08 18:27:31'),
(4, 'Alejandro Castro', 'giv-006', 'FDT  GFH', 1, 'users/default.png', 6, 6, 2, '2020-04-06 22:11:47', '2020-04-08 17:40:29'),
(5, 'Ricardo Lopez', 'giv-007', 'ksddj kfdkjfksd jfkdsjfk', 1, 'users/default.png', 7, 7, 1, '2020-04-08 19:44:16', '2020-04-08 19:48:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2020-03-29 19:58:38', '2020-03-29 19:58:38'),
(2, 'browse_bread', NULL, '2020-03-29 19:58:38', '2020-03-29 19:58:38'),
(3, 'browse_database', NULL, '2020-03-29 19:58:38', '2020-03-29 19:58:38'),
(4, 'browse_media', NULL, '2020-03-29 19:58:38', '2020-03-29 19:58:38'),
(5, 'browse_compass', NULL, '2020-03-29 19:58:39', '2020-03-29 19:58:39'),
(6, 'browse_menus', 'menus', '2020-03-29 19:58:39', '2020-03-29 19:58:39'),
(7, 'read_menus', 'menus', '2020-03-29 19:58:39', '2020-03-29 19:58:39'),
(8, 'edit_menus', 'menus', '2020-03-29 19:58:39', '2020-03-29 19:58:39'),
(9, 'add_menus', 'menus', '2020-03-29 19:58:39', '2020-03-29 19:58:39'),
(10, 'delete_menus', 'menus', '2020-03-29 19:58:39', '2020-03-29 19:58:39'),
(11, 'browse_roles', 'roles', '2020-03-29 19:58:39', '2020-03-29 19:58:39'),
(12, 'read_roles', 'roles', '2020-03-29 19:58:39', '2020-03-29 19:58:39'),
(13, 'edit_roles', 'roles', '2020-03-29 19:58:39', '2020-03-29 19:58:39'),
(14, 'add_roles', 'roles', '2020-03-29 19:58:39', '2020-03-29 19:58:39'),
(15, 'delete_roles', 'roles', '2020-03-29 19:58:39', '2020-03-29 19:58:39'),
(16, 'browse_users', 'users', '2020-03-29 19:58:39', '2020-03-29 19:58:39'),
(17, 'read_users', 'users', '2020-03-29 19:58:39', '2020-03-29 19:58:39'),
(18, 'edit_users', 'users', '2020-03-29 19:58:39', '2020-03-29 19:58:39'),
(19, 'add_users', 'users', '2020-03-29 19:58:39', '2020-03-29 19:58:39'),
(20, 'delete_users', 'users', '2020-03-29 19:58:39', '2020-03-29 19:58:39'),
(21, 'browse_settings', 'settings', '2020-03-29 19:58:39', '2020-03-29 19:58:39'),
(22, 'read_settings', 'settings', '2020-03-29 19:58:39', '2020-03-29 19:58:39'),
(23, 'edit_settings', 'settings', '2020-03-29 19:58:39', '2020-03-29 19:58:39'),
(24, 'add_settings', 'settings', '2020-03-29 19:58:39', '2020-03-29 19:58:39'),
(25, 'delete_settings', 'settings', '2020-03-29 19:58:39', '2020-03-29 19:58:39'),
(26, 'browse_hooks', NULL, '2020-03-29 19:58:41', '2020-03-29 19:58:41'),
(32, 'browse_contactos', 'contactos', '2020-04-08 18:04:08', '2020-04-08 18:04:08'),
(33, 'read_contactos', 'contactos', '2020-04-08 18:04:08', '2020-04-08 18:04:08'),
(34, 'edit_contactos', 'contactos', '2020-04-08 18:04:08', '2020-04-08 18:04:08'),
(35, 'add_contactos', 'contactos', '2020-04-08 18:04:08', '2020-04-08 18:04:08'),
(36, 'delete_contactos', 'contactos', '2020-04-08 18:04:08', '2020-04-08 18:04:08'),
(37, 'browse_suscriptors', 'suscriptors', '2020-04-08 18:13:19', '2020-04-08 18:13:19'),
(38, 'read_suscriptors', 'suscriptors', '2020-04-08 18:13:19', '2020-04-08 18:13:19'),
(39, 'edit_suscriptors', 'suscriptors', '2020-04-08 18:13:19', '2020-04-08 18:13:19'),
(40, 'add_suscriptors', 'suscriptors', '2020-04-08 18:13:19', '2020-04-08 18:13:19'),
(41, 'delete_suscriptors', 'suscriptors', '2020-04-08 18:13:19', '2020-04-08 18:13:19'),
(42, 'browse_pastors', 'pastors', '2020-04-08 19:00:47', '2020-04-08 19:00:47'),
(43, 'read_pastors', 'pastors', '2020-04-08 19:00:47', '2020-04-08 19:00:47'),
(44, 'edit_pastors', 'pastors', '2020-04-08 19:00:47', '2020-04-08 19:00:47'),
(45, 'add_pastors', 'pastors', '2020-04-08 19:00:47', '2020-04-08 19:00:47'),
(46, 'delete_pastors', 'pastors', '2020-04-08 19:00:47', '2020-04-08 19:00:47'),
(62, 'browse_bloques', 'bloques', '2020-04-08 20:01:47', '2020-04-08 20:01:47'),
(63, 'read_bloques', 'bloques', '2020-04-08 20:01:47', '2020-04-08 20:01:47'),
(64, 'edit_bloques', 'bloques', '2020-04-08 20:01:47', '2020-04-08 20:01:47'),
(65, 'add_bloques', 'bloques', '2020-04-08 20:01:47', '2020-04-08 20:01:47'),
(66, 'delete_bloques', 'bloques', '2020-04-08 20:01:47', '2020-04-08 20:01:47'),
(67, 'browse_causas', 'causas', '2020-04-08 20:02:15', '2020-04-08 20:02:15'),
(68, 'read_causas', 'causas', '2020-04-08 20:02:15', '2020-04-08 20:02:15'),
(69, 'edit_causas', 'causas', '2020-04-08 20:02:15', '2020-04-08 20:02:15'),
(70, 'add_causas', 'causas', '2020-04-08 20:02:15', '2020-04-08 20:02:15'),
(71, 'delete_causas', 'causas', '2020-04-08 20:02:15', '2020-04-08 20:02:15'),
(72, 'browse_contactenos', 'contactenos', '2020-04-08 20:02:49', '2020-04-08 20:02:49'),
(73, 'read_contactenos', 'contactenos', '2020-04-08 20:02:49', '2020-04-08 20:02:49'),
(74, 'edit_contactenos', 'contactenos', '2020-04-08 20:02:49', '2020-04-08 20:02:49'),
(75, 'add_contactenos', 'contactenos', '2020-04-08 20:02:49', '2020-04-08 20:02:49'),
(76, 'delete_contactenos', 'contactenos', '2020-04-08 20:02:49', '2020-04-08 20:02:49');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(32, 1),
(32, 2),
(33, 1),
(33, 2),
(34, 1),
(35, 1),
(36, 1),
(36, 2),
(37, 1),
(37, 2),
(38, 1),
(38, 2),
(39, 1),
(39, 2),
(40, 1),
(40, 2),
(41, 1),
(41, 2),
(42, 1),
(42, 2),
(43, 1),
(43, 2),
(44, 1),
(45, 1),
(46, 1),
(62, 1),
(62, 2),
(63, 1),
(63, 2),
(64, 1),
(64, 2),
(65, 1),
(66, 1),
(67, 1),
(67, 2),
(68, 1),
(68, 2),
(69, 1),
(69, 2),
(70, 1),
(71, 1),
(72, 1),
(72, 2),
(73, 1),
(73, 2),
(74, 1),
(74, 2),
(75, 1),
(76, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'super_admin', 'Super Administrador', '2020-03-29 19:58:38', '2020-03-29 20:19:56'),
(2, 'admin', 'Administrador Web', '2020-03-29 19:58:38', '2020-03-29 20:19:36'),
(3, 'pastor', 'Pastor', '2020-03-29 20:20:27', '2020-03-29 20:20:27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', 'settings\\March2020\\VQj4V2HqgQLNj5SviC8T.jpg', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Ofir', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Bienvenido al administrador de Ofir', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', 'settings\\March2020\\6Wulfx00YuSkqE8a7K53.png', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `suscriptors`
--

CREATE TABLE `suscriptors` (
  `id` int(10) UNSIGNED NOT NULL,
  `correo` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `suscriptors`
--

INSERT INTO `suscriptors` (`id`, `correo`, `created_at`, `updated_at`) VALUES
(1, 'zmacayo@gmail.com', '2020-04-08 18:20:26', '2020-04-08 18:20:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'superadmin', 'superadmin@ofir.com', 'users/default.png', NULL, '$2y$10$GyPMQuCntCuRciaLd0mx.OJmXrIvNWTHFM0/G6Dc504CaIgwqnCpi', '4WtO7lOeSPYGly1PaBQIGXdNXYCE61QkhGkkhDBt4bdEyRrWsMOKh4POaiLn', NULL, '2020-03-29 20:02:59', '2020-04-06 01:52:18'),
(2, 2, 'admin', 'admin@ofir.com', 'users/default.png', NULL, '$2y$10$D7qxvJm/FF5Mwr0Sl3GzkeSc712B7w7cD21F8HIoD0p0YvsrIXCV2', 'dFxR6HGeGEAl8Z49za7bH7HCEsuYSQsovBc09JjhqaAOuANR8en7D0k2XjBk', NULL, '2020-03-29 20:08:57', '2020-03-29 20:08:57'),
(3, 3, 'Carlos fernandez', 'carlos_1@gmail.com', 'users/1586300929.default.png', NULL, '$2y$10$SyDtpk7FFnxur26rdMz3c.WElIQYXaLu4C5C4BeSXtpwx/3So0dk6', 'Uu1LQ92ehqJEI2ahtmFM0LkBjlzloLr5q1KutFecquvZKDh7VLaTSUSlG5Pj', NULL, '2020-04-03 22:12:59', '2020-04-08 04:10:24'),
(4, 3, 'Carlos fer', 'carlos@gmail.com', 'users/default.png', NULL, '$2y$10$LQ.qYWBhe6RcVESuZFzCJO/mt22BO8sBQbjVTFPdZyXtOmQiK3JuK', '77FIXw4viu55o42qvW26AtrRKQOw2Pf4EyGnNzDgPhUHfrdo6lzWSxvz1DOw', NULL, '2020-04-06 22:06:27', '2020-04-06 22:06:27'),
(5, 3, 'Luis Reyes', 'luis@kd.com', 'iglesia.jpg', NULL, '$2y$10$JFXL509.rfrKBRIcSdK8Euav63Pk99mozIC43henV75cRccuGrDla', 'Qk8M5eFyJ3nVM3znSvIMeM3n1PqV5Owtm2kDtKIT7aH3FadHNXIplyzLXn9v', NULL, '2020-04-06 22:09:34', '2020-04-06 22:09:34'),
(6, 3, 'Alejandro Castro', 'alejndro@gmail.com', 'users/default.png', NULL, '$2y$10$uE72Qw20Rg/YhbRIFprrv.9E6wSjeLrX8.wA4vuTFzHgOrckcxNfq', 'pZDox3DYlQ6lcfZ454aGTUyZiXV7w8GiCcDXvHnxzzFSDUQE5aEBPoG4WbRj', NULL, '2020-04-06 22:11:47', '2020-04-06 22:11:47'),
(7, 3, 'Ricardo Lopez', 'ricardo@gmail.com', 'users/default.png', NULL, '$2y$10$6wFBJqPQbXcB2JZdKbvF0u4PplhATf9/t/ht/7bhLUHGAGxVOg5PG', 'KWdf2B8F4FLNs8i9Df0ung34oGzslqnDrJq0tisT3GIjT02s2O0MVW7NcBkg', NULL, '2020-04-08 19:44:16', '2020-04-08 19:44:16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `user_roles`
--

INSERT INTO `user_roles` (`user_id`, `role_id`) VALUES
(2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `visitas`
--

CREATE TABLE `visitas` (
  `id` int(10) UNSIGNED NOT NULL,
  `fecha` date NOT NULL,
  `pastor_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `visitas`
--

INSERT INTO `visitas` (`id`, `fecha`, `pastor_id`, `created_at`, `updated_at`) VALUES
(1, '2020-04-07', 1, '2020-04-07 19:32:33', '2020-04-07 19:32:33'),
(2, '2020-04-07', 1, '2020-04-08 04:03:47', '2020-04-08 04:03:47'),
(3, '2020-04-07', 1, '2020-04-08 04:04:35', '2020-04-08 04:04:35'),
(4, '2020-04-07', 1, '2020-04-08 04:04:47', '2020-04-08 04:04:47'),
(5, '2020-04-08', 1, '2020-04-08 17:29:41', '2020-04-08 17:29:41'),
(6, '2020-04-08', 1, '2020-04-08 17:30:32', '2020-04-08 17:30:32'),
(7, '2020-04-08', 1, '2020-04-08 17:31:24', '2020-04-08 17:31:24'),
(8, '2020-04-08', 1, '2020-04-08 17:31:42', '2020-04-08 17:31:42'),
(9, '2020-04-08', 1, '2020-04-08 17:34:23', '2020-04-08 17:34:23'),
(10, '2020-04-08', 1, '2020-04-08 17:35:57', '2020-04-08 17:35:57'),
(11, '2020-04-08', 3, '2020-04-08 17:36:33', '2020-04-08 17:36:33'),
(12, '2020-04-08', 4, '2020-04-08 17:36:42', '2020-04-08 17:36:42'),
(13, '2020-04-08', 2, '2020-04-08 17:36:50', '2020-04-08 17:36:50'),
(14, '2020-04-08', 4, '2020-04-08 17:40:29', '2020-04-08 17:40:29'),
(15, '2020-04-08', 3, '2020-04-08 18:27:31', '2020-04-08 18:27:31'),
(16, '2020-04-08', 5, '2020-04-08 19:48:05', '2020-04-08 19:48:05');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `bloques`
--
ALTER TABLE `bloques`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `causas`
--
ALTER TABLE `causas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contactenos`
--
ALTER TABLE `contactenos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contactos`
--
ALTER TABLE `contactos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indices de la tabla `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indices de la tabla `iglesias`
--
ALTER TABLE `iglesias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indices de la tabla `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `pastors`
--
ALTER TABLE `pastors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pastors_user_id_foreign` (`user_id`),
  ADD KEY `pastors_iglesia_id_foreign` (`iglesia_id`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indices de la tabla `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indices de la tabla `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indices de la tabla `suscriptors`
--
ALTER TABLE `suscriptors`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indices de la tabla `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- Indices de la tabla `visitas`
--
ALTER TABLE `visitas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `visitas_pastor_id_foreign` (`pastor_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `bloques`
--
ALTER TABLE `bloques`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `causas`
--
ALTER TABLE `causas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `contactenos`
--
ALTER TABLE `contactenos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `contactos`
--
ALTER TABLE `contactos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT de la tabla `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `iglesias`
--
ALTER TABLE `iglesias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT de la tabla `pastors`
--
ALTER TABLE `pastors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `suscriptors`
--
ALTER TABLE `suscriptors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `visitas`
--
ALTER TABLE `visitas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `pastors`
--
ALTER TABLE `pastors`
  ADD CONSTRAINT `pastors_iglesia_id_foreign` FOREIGN KEY (`iglesia_id`) REFERENCES `iglesias` (`id`),
  ADD CONSTRAINT `pastors_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Filtros para la tabla `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `visitas`
--
ALTER TABLE `visitas`
  ADD CONSTRAINT `visitas_pastor_id_foreign` FOREIGN KEY (`pastor_id`) REFERENCES `pastors` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
